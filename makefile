
#
#
#   makefile: the root makefile that powers the GNU make
#             build system.
#
#   Copyright (C) 2018 Sandro Francesco Montemezzani
#   Author: Sandro Montemezzani <sandro@montemezzani.net>
#
#   This file is part of the SFM-OS project, and is licensed
#   under the MIT License.
#
#


#################
#  BASIC SETUP  #
#################

# version setup
PROJECT       := SFM-OS
PROJECT_LINK  := https://github.com/saframon/SFM-OS
VERSION_MAJOR := 0
VERSION_MINOR := 1
VERSION_PATCH := 3

# setup the shell
SHELL = /bin/bash

# prevent make`s `Entering directory...' message
MAKEFLAGS += --no-print-directory

# handle the verbosity of the build stage
ifdef V
  ifeq ("$(origin V)","command line")
    BUILD_VERBOSE = $(V)
  endif
endif
ifndef BUILD_VERBOSE
  BUILD_VERBOSE = 0
endif

# check how many processors are online
NPROCS := $(shell getconf _NPROCESSORS_ONLN || echo 4)


#########################
#  HOST COMPILER SETUP  #
#########################

ifdef CC
  ifeq ("$(origin CC)","command line")
    HOST_CC := $(CC)
  endif
endif
ifndef HOST_CC
  HOST_CC := gcc
endif
ifdef CFLAGS
  ifeq ("$(origin CFLAGS)","command line")
    HOST_CFLAGS := $(CFLAGS)
  endif
endif
ifndef HOST_CFLAGS
  HOST_CFLAGS := -Wall -O2 -fomit-frame-pointer
endif
ifdef CXX
  ifeq ("$(origin CXX)","command line")
    HOST_CXX := $(CXX)
  endif
endif
ifndef HOST_CXX
  HOST_CXX := g++
endif
ifdef CXXFLAGS
  ifeq ("$(origin CXXFLAGS)","command line")
    HOST_CXXFLAGS := $(CXXFLAGS)
  endif
endif
ifndef HOST_CXXFLAGS
  HOST_CXXFLAGS := -O2
endif


#############################
#  OUPUT BEAUTIFYING SETUP  #
#############################

ifeq ($(BUILD_VERBOSE),1)
  quiet = 
  Q = 
else
  quiet = q
  Q = @
endif
ifneq ($(findstring s,$(MAKEFLAGS)),)
  quiet = silent
endif


#########################
#  DIRECTORY VARIABLES  #
#########################

kerneldir    := $(CURDIR)/kernel
libstddir    := $(CURDIR)/libstd
loaderdir    := $(CURDIR)/loader
scriptsdir   := $(CURDIR)/scripts


#####################
#  TOOLCHAIN SETUP  #
#####################

# basic architecture variables
PREFIX			?= $(CURDIR)/deploy
ARCH            ?= x86
TOOLCHAIN       ?= $(CURDIR)/toolchains/$(ARCH)-toolchain
CROSS_COMPILE   ?= $(TOOLCHAIN)/bin/$(ARCH)-

# define the cross compiler variables
CROSS_AR      = $(CROSS_COMPILE)ar
CROSS_AS      = $(CROSS_COMPILE)as
CROSS_CC      = $(CROSS_COMPILE)gcc
CROSS_CPP     = $(CROSS_CC) -E
CROSS_LD      = $(CROSS_COMPILE)ld
CROSS_NM      = $(CROSS_COMPILE)nm
CROSS_OBJCOPY = $(CROSS_COMPILE)objcopy
CROSS_OBJDUMP = $(CROSS_COMPILE)objdump
CROSS_RANLIB  = $(CROSS_COMPILE)ranlib
CROSS_STRIP   = $(CROSS_COMPILE)strip

# gcc support functions
try-run = $(shell set -e; \
                  TMP="$$$$.tmp"; \
                  if ($(1)) >/dev/null 2>&1; \
                  then echo "$(2)"; else echo "$(3)"; \
                  fi; rm -rf "$$TMP")
as-option = $(call try-run,$(CC) -c -o "$$TMP" \
	-xassembler /dev/null $(1:%=-Xassembler %),$(1),$(2))
cc-option = $(call try-run,$(CC) $(1) -c -o "$$TMP" \
	-xc /dev/null,$(1),$(2))
ld-option = $(call try-run,$(CC) $(CFLAGS) -nostdlib -o "$$TMP" \
	-xc /dev/null $(1:%=-Xlinker %),$(1),$(2))

# compiler options
CROSS_ARFLAGS  := 
CROSS_ASFLAGS  := -I$(libstddir)/include
CROSS_CFLAGS   := -pipe -O2 -std=c99 -Wall -Wextra
CROSS_CPPFLAGS := -D__SFMOS__ -D__$(ARCH)__ -isystem $(libstddir)/include
CROSS_LDFLAGS  := -L$(libstddir)/lib --nmagic -static -gc-sections


##################
#  BUILD CONFIG  #
##################

SFMOS_BUILD_CONFIG_DEBUG ?= 1
SFMOS_BUILD_CONFIG_LTO   ?= 1


#############################
#  UPDATE COMPILER OPTIONS  #
#############################

ifeq ($(SFMOS_BUILD_CONFIG_DEBUG),1)
  CROSS_CFLAGS += -g
endif

ifeq ($(SFMOS_BUILD_CONFIG_LTO),1)
  CROSS_AR     = $(CROSS_COMPILE)gcc-ar
  CROSS_NM     = $(CROSS_COMPILE)gcc-nm
  CROSS_RANLIB = $(CROSS_COMPILE)gcc-ranlib
  CROSS_CFLAGS += -flto=$(NPROCS) -fno-fat-lto-objects -fwhole-program
endif


#################
#  FINAL SETUP  #
#################

# export all variables
export VERSION_MAJOR VERSION_MINOR VERSION_PATCH
export SHELL NPROCS quiet Q
export HOST_CC HOST_CFLAGS HOST_CXX HOST_CXXFLAGS
export PREFIX ARCH TOOLCHAIN CROSS_COMPILE
export CROSS_AR CROSS_AS CROSS_CC CROSS_CPP CROSS_LD CROSS_NM
export CROSS_OBJCOPY CROSS_OBJDUMP CROSS_RANLIB CROSS_STRIP
export CROSS_ARFLAGS CROSS_ASFLAGS CROSS_CFLAGS CROSS_CPPFLAGS CROSS_LDFLAGS
export SFMOS_BUILD_CONFIG_DEBUG SFMOS_BUILD_CONFIG_LTO

# create the `cmd' variable
cmd = @$(if $($(quiet)cmd_$(1)),echo "$($(quiet)cmd_$(1))" &&) $(cmd_$(1))


##########################
#  ALL - DEFAULT TARGET  #
##########################

PHONY := all
all: kernel libstd


###################
#  CLEAN TARGETS  #
###################

PHONY += clean
clean:
	$(Q)$(MAKE) -C $(kerneldir) clean
	$(Q)$(MAKE) -C $(libstddir) clean
	$(Q)$(MAKE) -C $(loaderdir) clean

PHONY += distclean
distclean:
	$(Q)$(MAKE) -C $(kerneldir) distclean
	$(Q)$(MAKE) -C $(libstddir) distclean
	$(Q)$(MAKE) -C $(loaderdir) distclean

PHONY += mostlyclean
mostlyclean:
	$(Q)$(MAKE) -C $(kerneldir) clean
	$(Q)$(MAKE) -C $(libstddir) clean


###################
#  KERNEL TARGET  #
###################

PHONY += kernel
kernel: libstd
	$(Q)$(MAKE) -C $(kerneldir) kernel binary


###################
#  LIBSTD TARGET  #
###################

PHONY += libstd
libstd:
	$(Q)$(MAKE) -C $(libstddir) libstd


###################
#  LOADER TARGET  #
###################

PHONY += loader
loader:
	$(Q)$(MAKE) -C $(loaderdir) loader binary


#####################
#  INSTALL TARGETS  #
#####################

PHONY += install-kernel
install-kernel:
	$(Q)$(MAKE) -C $(kerneldir) install

PHONY += install-libstd
install-libstd:
	$(Q)$(MAKE) -C $(libstddir) install

PHONY += install-loader
install-loader:
	$(Q)$(MAKE) -C $(loaderdir) install

PHONY += install
install: install-kernel install-libstd


#################
#  TEST TARGET  #
#################

PHONY += test
test:
	$(Q)$(MAKE) -C $(libstddir) test


#################
#  CHECK TARGET  #
#################

PHONY += check
check:
	$(Q)$(MAKE) -C $(libstddir) check


################
#  COV TARGET  #
################

PHONY += cov
cov:
	$(Q)$(MAKE) -C $(libstddir) cov


######################
#  TOOLCHAIN TARGET  #
######################

TOOLCHAIN_SCRIPT := $(scriptsdir)/toolchain.sh

qcmd_toolchain = TOOLCHAIN $(2)
cmd_toolchain  = $(TOOLCHAIN_SCRIPT) $(2)

PHONY += toolchain
toolchain:
	+$(call cmd,toolchain,check)

PHONY += build-toolchain
build-toolchain:
	+$(call cmd,toolchain,build)
	+$(call cmd,toolchain,pack)


#################
#  HELP TARGET  #
#################

PHONY += help
help:
	@echo "$(PROJECT) v$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)"
	@echo "$(PROJECT_LINK)"
	@echo
	@echo "CLEAN TARGETS:"
	@echo "  make clean                  remove some temporary build files"
	@echo "  make distclean              reset the project directory"
	@echo
	@echo "BUILD TARGETS:"
	@echo "  make [all] <ARGS>           build all targets prefixed with a '*'"
	@echo " *make kernel <ARGS>          build the kernel binary"
	@echo " *make libstd <ARGS>          build the C standard library"
	@echo "  make loader <ARGS>          build the bootloader binary"
	@echo
	@echo "INSTALL TARGETS:"
	@echo "  make install <ARGS>         install all default build targets"
	@echo "  make install-kernel <ARGS>  install the kernel binary"
	@echo "  make install-libstd <ARGS>  install the C standard library"
	@echo "  make install-loader <ARGS>  install the bootloader binary"
	@echo
	@echo "SETUP TARGETS:"
	@echo "  make toolchain <ARGS>       download/compile the GCC cross" \
		"compiler toolchain required for the various compilation steps"
	@echo "  make build-toolchain <ARGS> force compile the GCC cross" \
		"compiler toolchain required for the various compilation steps"
	@echo
	@echo "HELP TARGETS:"
	@echo "  make help                   prints this help page"
	@echo
	@echo "POSSIBLE ARGUMENTS:"
	@echo "  V=                verbosity level. 0=normal, 1=verbose"
	@echo "  PREFIX=           install path. defaults to \"./deploy\""
	@echo "  ARCH=             build architecture. One out of arm,x86"
	@echo "  TOOLCHAIN=        toolchain path. defaults to" \
		"\"./toolchain/<ARCH>-toolchain\""
	@echo "  CROSS_COMPILE=    compiler prefix incl. path. defaults to" \
		"\"<TOOLCHAIN>/bin/<ARCH>-\""
	@echo
	@echo "EXAMPLE INVOKATIONS:"
	@echo "  make ARCH=x86"
	@echo "  make loader ARCH=arm V=1"
	@echo


###################
#  PHONY TARGETS  #
###################

PHONY += FORCE
FORCE:

.PHONY: $(PHONY)

