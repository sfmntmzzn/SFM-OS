#!/bin/bash

# check the current environment state
[ -z "$NPROCS" ] && NPROCS="$(getconf _NPROCESSORS_ONLN || echo 4)"
[ -z "$HOST_CC" ] && HOST_CC="${CC:=gcc}"
[ -z "$HOST_CXX" ] && HOST_CXX="${CXX:=g++}"
[ -z "$ARCH" ] && echo "\$ARCH not available!" && exit 1
[ -z "$TOOLCHAIN" ] && echo "\$TOOLCHAIN not available!" && exit 1

# basic script variables
BUGURL="https://github.com/saframon/SFM-OS/issues"
UNAME_S=$(uname -s)
UNAME_M=$(uname -m)

# switch the target ARCH
case "$ARCH" in
	arm) BINUTILS_TRIPLET="arm-eabi" GCC_TRIPLET="arm-eabi" ;;
	i?86|x86) BINUTILS_TRIPLET="i386-elf" GCC_TRIPLET="i386-elf" ;;
	*) echo "unsupported architecture \"$ARCH\""; exit 1 ;;
esac

# version configuration
BINUTILS_VERSION="binutils-2.29"
LIBICONV_VERSION="libiconv-1.15"
MPFR_VERSION="mpfr-4.0.0"
GMP_VERSION="gmp-6.1.2"
MPC_VERSION="mpc-1.1.0"
ISL_VERSION="isl-0.18"
CLOOG_VERSION="cloog-0.18.1"
GCC_VERSION="gcc-7.2.0"

# binutils configuration
BINUTILS_CONFIG="--with-sysroot --disable-nls --disable-multilib"
BINUTILS_CONFIG+=" --enable-gold --disable-ld --enable-lto --enable-plugins"
BINUTILS_CONFIG+=" --disable-werror"

# gcc configuration
GCC_CONFIG="--disable-nls --without-headers"
GCC_CONFIG+=" --disable-multiarch --disable-multilib"
GCC_CONFIG+=" --enable-gold --disable-ld"
GCC_CONFIG+=" --enable-languages=c --enable-lto --enable-checking=release"
GCC_CONFIG+=" --disable-werror"

# host compiler configuration
HOST_CFLAGS+=" -Os"
HOST_CXXFLAGS+=" -Os"

# cross platform MD5 hash function
function md5hash {
	case "$UNAME_S" in
		Linux) echo -n "$1" | md5sum | cut -d " " -f 1 ;;
		Darwin) echo -n "$1" | md5 ;;
	esac
}

# populate the checksum string variable
CHECKSUM_STRING="SFM-OS,$ARCH"
CHECKSUM_STRING+=",$BINUTILS_VERSION"
CHECKSUM_STRING+=",$LIBICONV_VERSION"
CHECKSUM_STRING+=",$MPFR_VERSION"
CHECKSUM_STRING+=",$GMP_VERSION"
CHECKSUM_STRING+=",$MPC_VERSION"
CHECKSUM_STRING+=",$ISL_VERSION"
CHECKSUM_STRING+=",$CLOOG_VERSION"
CHECKSUM_STRING+=",$GCC_VERSION"

# download and extract source archive
function download_source {
	mkdir -p "$2"
	curl -L -o "$2/${1##*/}" -O "$1"
	if [ $? -ne 0 ];
	then
		echo "Download error: \"$1\"";
		return 1;
	fi
	if [ -n "$3" ];
	then
		curl -L -o "$2/${1##*/}.sig" -O "$1.sig";
		if [ $? -ne 0 ];
		then
			echo "Download error: \"$1.sig\"";
			return 1;
		fi;
		gpg --verify --keyring "$3" "$2/${1##*/}.sig";
		if [ $? -ne 0 ];
		then
			echo "Signature mismatch: \"$2/${1##*/}\"";
			return 1;
		fi;
	fi
	tar -xf "$2/${1##*/}" -C "$2"
	if [ $? -ne 0 ];
	then
		echo "Unpack error: \"$2/${1##*/}\"";
		return 1;
	fi
	rm "$2/${1##*/}"
	if [ -n "$3" ];
	then
		rm "$2/${1##*/}.sig";
	fi
}

# build toolchain
function build_toolchain {
	local BLDDIR="$TOOLCHAIN.build"
	local SRCDIR="$TOOLCHAIN.source"
	local KEYRING="$SRCDIR/gnu-keyring.gpg"
	local GNUFTP="https://ftpmirror.gnu.org"
	local INFRASTRUCFTP="ftp://gcc.gnu.org/pub/gcc/infrastructure"
	export PATH="$TOOLCHAIN/bin:$PATH"
	[ -d "$TOOLCHAIN" ] && rm -r "$TOOLCHAIN"
	mkdir -p "$TOOLCHAIN"
	[ -d "$SRCDIR" ] || mkdir -p "$SRCDIR"
	curl -L -o "$KEYRING" -O "$GNUFTP/gnu-keyring.gpg"
	if [ $? -ne 0 ];
	then
		echo "Download error: gnu-keyring.gpg";
		return 1;
	fi
	[ -d "$SRCDIR/$BINUTILS_VERSION" ] || ( download_source \
		"$GNUFTP/binutils/$BINUTILS_VERSION.tar.xz" "$SRCDIR/" \
		"$KEYRING" || return 100 )
	mkdir -p "$BLDDIR/binutils"
	pushd "$BLDDIR/binutils"
	"$SRCDIR/$BINUTILS_VERSION/configure" \
		CC="$HOST_CC" CFLAGS="$HOST_CFLAGS" \
		CXX="$HOST_CXX" CXXFLAGS="$HOST_CXXFLAGS" \
		--prefix="$TOOLCHAIN" \
		--target="$BINUTILS_TRIPLET" \
		--program-prefix="$ARCH-" \
		--program-suffix="" \
		$BINUTILS_CONFIG \
		--with-pkgversion="SFM-OS Toolchain Binutils" \
		--with-bugurl="$BUGURL"
	if [ $? -ne 0 ];
	then
		echo "Configuration error: binutils";
		popd;
		return 101;
	fi
	make -j$NPROCS
	if [ $? -ne 0 ];
	then
		echo "Build error: binutils";
		popd;
		return 102;
	fi
	make install
	if [ $? -ne 0 ];
	then
		echo "Install error: binutils";
		popd;
		return 103;
	fi
	popd
	[ -d "$SRCDIR/$GCC_VERSION" ] || ( download_source \
		"$GNUFTP/gcc/$GCC_VERSION/$GCC_VERSION.tar.gz" "$SRCDIR/" \
		"$KEYRING" || return 200 )
	[ -d "$SRCDIR/$LIBICONV_VERSION" ] || ( download_source \
		"$GNUFTP/libiconv/$LIBICONV_VERSION.tar.gz" "$SRCDIR/" \
		"$KEYRING" || return 200; ln -sf "$SRCDIR/$LIBICONV_VERSION" \
		"$SRCDIR/$GCC_VERSION/libiconv"; )
	[ -d "$SRCDIR/$MPFR_VERSION" ] || ( download_source \
		"$GNUFTP/mpfr/$MPFR_VERSION.tar.xz" "$SRCDIR/" "$KEYRING" \
		|| return 200; ln -sf "$SRCDIR/$MPFR_VERSION" \
		"$SRCDIR/$GCC_VERSION/mpfr"; )
	[ -d "$SRCDIR/$GMP_VERSION" ] || ( download_source \
		"$GNUFTP/gmp/$GMP_VERSION.tar.xz" "$SRCDIR/" "$KEYRING" \
		|| return 200; ln -sf "$SRCDIR/$GMP_VERSION" \
		"$SRCDIR/$GCC_VERSION/gmp"; )
	[ -d "$SRCDIR/$MPC_VERSION" ] || ( download_source \
		"$GNUFTP/mpc/$MPC_VERSION.tar.gz" "$SRCDIR/" "$KEYRING" \
		|| return 200; ln -sf "$SRCDIR/$MPC_VERSION" \
		"$SRCDIR/$GCC_VERSION/mpc"; )
	[ -d "$SRCDIR/$ISL_VERSION" ] || ( download_source \
		"$INFRASTRUCFTP/$ISL_VERSION.tar.bz2" "$SRCDIR/" \
		|| return 200; ln -sf "$SRCDIR/$ISL_VERSION" \
		"$SRCDIR/$GCC_VERSION/isl"; )
	[ -d "$SRCDIR/$CLOOG_VERSION" ] || ( download_source \
		"$INFRASTRUCFTP/$CLOOG_VERSION.tar.gz" "$SRCDIR/" \
		|| return 200; ln -sf "$SRCDIR/$CLOOG_VERSION" \
		"$SRCDIR/$GCC_VERSION/cloog"; )
	mkdir -p "$BLDDIR/gcc"
	pushd "$BLDDIR/gcc"
	"$SRCDIR/$GCC_VERSION/configure" \
		CC="$HOST_CC" CFLAGS="$HOST_CFLAGS" \
		CXX="$HOST_CXX" CXXFLAGS="$HOST_CXXFLAGS" \
		--prefix="$TOOLCHAIN" \
		--target="$GCC_TRIPLET" \
		--program-prefix="$ARCH-" \
		--program-suffix="" \
		$GCC_CONFIG \
		--with-pkgversion="SFM-OS Toolchain GCC" \
		--with-bugurl="$BUGURL"
	if [ $? -ne 0 ];
	then
		echo "Configuration error: gcc";
		popd;
		return 201;
	fi
	make -j$NPROCS all-gcc
	if [ $? -ne 0 ];
	then
		echo "Build error: gcc";
		popd;
		return 202;
	fi
	make install-gcc
	if [ $? -ne 0 ];
	then
		echo "Install error: gcc";
		popd;
		return 203;
	fi
	make -j$NPROCS all-target-libgcc
	if [ $? -ne 0 ];
	then
		echo "Build error: libgcc";
		popd;
		return 204;
	fi
	make install-target-libgcc
	if [ $? -ne 0 ];
	then
		echo "Install error: libgcc";
		popd;
		return 205;
	fi
	popd "$STARTDIR"
	rm -rf "$BLDDIR" "$SRCDIR"
	echo $CHECKSUM_STRING > "$TOOLCHAIN/$(md5hash "$CHECKSUM_STRING")"
}

# pack toolchain
function pack_toolchain {
	tar -cjf "$TOOLCHAIN.$UNAME_S.$UNAME_M.tar.bz2" -C "$TOOLCHAIN" .
	if [ $? -ne 0 ];
	then
		echo "Pack error: \"$TOOLCHAIN.$UNAME_S.$UNAME_M.tar.bz2\"";
		return 1;
	fi
}

# get remote package link
function remotelink {
	local DBBASE="https://dl.dropboxusercontent.com/s"
	local DBKEY=""
	case "$1" in
		x86-toolchain.Linux.x86_64.tar.bz2)
			DBKEY="lfdf3h9a7ekjzl5" ;;
		x86-toolchain.Darwin.x86_64.tar.bz2)
			DBKEY="ellj9k6mtxowod2" ;;
		*)
			return 1 ;;
	esac
	echo $DBBASE/$DBKEY/$1
	return 0
}

# download toolchain
function download_toolchain {
	local PACKAGE_NAME="$ARCH-toolchain.$UNAME_S.$UNAME_M.tar.bz2"
	local PACKAGE_LINK="$(remotelink "$PACKAGE_NAME")"
	local PACKAGE_LOCAL="$TOOLCHAIN/$ARCH-toolchain.tar.bz2"
	if [ -n "$PACKAGE_LINK" ];
	then
		mkdir -p "$TOOLCHAIN"
		curl -L -o "$PACKAGE_LOCAL" -O "$PACKAGE_LINK"
		if [ $? -ne 0 ];
		then
			echo "Download error: \"$PACKAGE_LINK\"";
			return 1;
		fi;
		tar -xf "$PACKAGE_LOCAL" -C "$TOOLCHAIN"
		if [ $? -ne 0 ];
		then
			echo "Unpack error: \"$PACKAGE_LOCAL\"";
			return 1;
		fi;
		rm "$PACKAGE_LOCAL"
		if [ -f "$TOOLCHAIN/$(md5hash "$CHECKSUM_STRING")" ];
		then
			echo "Update complete! Toolchain up to date."
			return 0;
		else
			echo "Checksum mismatch! Toolchain still not" \
				"up to date.";
			echo "Maybe the online version isn't" \
				"up to date either.";
			return 1;
		fi
	else
		echo "No predefined URL found for \"$PACKAGE_NAME\"!";
		echo "You might want to build the toolchain by yourself."
		read -r -t 60 -p "Build toolchain? [y/N]" ANSWER;
		case "$ANSWER" in
			y|Y)
				build_toolchain;
				return $? ;;
			*)
				return 1 ;;
		esac
	fi
}

# check toolchain
function check_toolchain {
	if [ -d "$TOOLCHAIN" ];
	then
		if [ -f $TOOLCHAIN/$(md5hash "$CHECKSUM_STRING") ];
		then
			echo "Good. The toolchain is up to date.";
			return 0;
		else
			echo "The toolchain needs to be updated!"
			read -r -t 60 -p "Update now? [Y/n]" ANSWER;
			case "$ANSWER" in
				''|y|Y)
					[ -d "$TOOLCHAIN" ] \
						&& rm -rf "$TOOLCHAIN";
					download_toolchain;
					return $? ;;
				*)
					return 1 ;;
			esac
		fi
	else
		download_toolchain;
		return $?;
	fi
}

# parse the command line
case "$1" in
	build) build_toolchain; exit $? ;;
	pack) pack_toolchain; exit $? ;;
	download) download_toolchain; exit $? ;;
	check) check_toolchain; exit $? ;;
	*) echo "unknown command: \"$1\""; exit 1 ;;
esac
shift

# exit
exit $?

