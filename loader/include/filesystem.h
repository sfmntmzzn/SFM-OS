
/* ----------------------------------------------------------------- *
 *
 *   filesystem.h: various definitions and macros regarding
 *                 file systems.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LOADER_FILESYSTEM_HEADER
#define	LOADER_FILESYSTEM_HEADER


#if LOADER_FS == FAT12
#define	read_file	fat12_read_file
#else
#error unsupported filesystem
#endif // LOADER_FS


#endif // LOADER_FILESYSTEM_HEADER

