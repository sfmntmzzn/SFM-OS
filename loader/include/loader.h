
/* ----------------------------------------------------------------- *
 *
 *   loader.h: general macros and declarations for the loader.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LOADER_LOADER_HEADER
#define LOADER_LOADER_HEADER


#ifndef __ASSEMBLER__

#define BOOT1_FUNC	__attribute__((section(".boot1"),cdecl,regparm(0)))
#define BOOT1_DATA	__attribute__((section(".boot1.data"),aligned(1)))


#define UNLIKELY(x)	__builtin_expect(!!(x),0)
#define LIKELY(x)	__builtin_expect(!!(x),1)

#endif // __ASSEMBLER__


#endif // LOADER_LOADER_HEADER

