
/* ----------------------------------------------------------------- *
 *
 *   version.c: provides a string variable containing version data
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <loader.h>
#include <version.h>


BOOT1_DATA char __version_str1__[] =
	"Loader for SFMOS " LOADER_VERSION_STRING;

BOOT1_DATA char __version_str2__[] =
	"Build by " LOADER_BUILD_INVOKER
	" using " LOADER_BUILD_COMPILER
	" on " LOADER_BUILD_HOST;

