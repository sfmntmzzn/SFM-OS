
/* ----------------------------------------------------------------- *
 *
 *   copy_memory.c: provides a function to copy memory to
 *                  another location, much like `memcpy'
 *                  from the standard library.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <loader.h>


BOOT1_FUNC void* copy_memory ( void* dest, const void* src, unsigned int len )
{
	// create the temporary pointer variables so
	// we don't have to modify the arguments
	char* d = (char*)( dest );
	const char* s = (const char*)( src );

	// loop through every byte in the specified
	// memory area
	for ( unsigned int i = 0; i != len; ++i )
	{
		// copy the byte over to the
		// destination byte
		d[ i ] = s[ i ];
	}

	// return the original destination pointer
	return dest;
}

