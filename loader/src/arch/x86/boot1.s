
/* ----------------------------------------------------------------- *
 *
 *   boot1.s: boot1 (or stage2) loader code
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


// ---------------------------------------------------------------------
// |   MAP OF BOTTOM HALF OF CONVENTIONAL MEMORY WHEN ENTERING BOOT1   |
// ---------------------------------------------------------------------

	// 00000..003FF: interrupt vector table
	// 00400..004FF: BIOS data area
	// 00500..07BFF: real mode stack
	// 07C00..07DFF: boot sector
	// 07E00..07FFF: free memory
	// 08000..0ABFF: loaded boot1 code
	// 0AC00..9FFFF: free memory
	// A0000..AFFFF: graphics video memory
	// B0000..B7FFF: monochrome text memory
	// B8000..BFFFF: colored text memory
	// C0000..FFFEF: ROM code area


#include <filesystem.h>

	.set	mbi_memory_base, 0x00000500
	.set	kernel_memory_base, 0x00010000

.extern __boot1
.extern __version_str1__
.extern __version_str2__
.extern a20
.extern boot0.bpb
.extern copy_memory
.extern e801
.extern e820
.extern int12h
.extern multiboot_handle
.extern protected_putc
.extern protected_puts
.extern read_file
.extern real_putc
.extern real_puts

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global boot1
.type boot1, @function

boot1:
	// ------------------------------
	// |   BOOT1 - STAGE 2 LOADER   |
	// ------------------------------

	// clear all general purpose registers
	xor	%ax, %ax
	xor	%bx, %bx
	xor	%cx, %cx
	xor	%dx, %dx
	xor	%si, %si
	xor	%di, %di

	// next, ensure we are in VGA text mode during the boot process
	mov	$0x03, %ax	/* AL=0x03: VGA colored text mode */
	int	$0x10

	// print the version information for the build of
	// this bootloader to the screen
	push	$__version_str1__
	call	real_puts
	push	$'\n'
	call	real_putc
	push	$__version_str2__
	call	real_puts
	add	$2, %sp
	call	real_putc
	call	real_putc
	add	$4, %sp

	// print the enable A20 message
	push	$a20_message
	call	real_puts
	add	$2, %sp

	// enable A20 gate, so we can use addresses above 1MB
	call	a20
	test	%ax, %ax	/* 0 means success, else failure */
	jnz	boot1.halt16

	// print the done message
	push	$done_message
	call	real_puts
	add	$2, %sp

	// print the determine lower memory message
	push	$lower_memory_message
	call	real_puts
	add	$2, %sp

	// determine lower memory bounds
	call	int12h
	test	%ax, %ax	/* 0 means success, else failure */
	jnz	boot1.halt16

	// print the done message
	push	$done_message
	call	real_puts
	add	$2, %sp

	// print the determine upper memory message
	push	$upper_memory_message
	call	real_puts
	add	$2, %sp

	// determine upper memory bounds
	call	e801
	test	%ax, %ax	/* 0 means success, else failure */
	jnz	boot1.halt16

	// print the done message
	push	$done_message
	call	real_puts
	add	$2, %sp

	// print the load mmap message
	push	$mmap_message
	call	real_puts
	add	$2, %sp

	// read the mmap structre into memory
	call	e820
	test	%ax, %ax	/* 0 means success, else failure */
	jnz	boot1.halt16

	// print the done message
	push	$done_message
	call	real_puts
	add	$2, %sp

	// print the read kernel binary message
	push	$kernel_read_message
	call	real_puts
	add	$2, %sp

	// read the kernel binary into memory at location
	// 0x1000:0x0000
	push	$(kernel_memory_base & 0x0F)	/* param: offset */
	push	$(kernel_memory_base >> 4)	/* param: segment */
	push	$kernel_file			/* param: filename */
	xor	%ax, %ax
	movb	(boot0.bpb + 0x24), %al
	push	%ax				/* param: drive */
	call	read_file
	add	$8, %sp
	test	%ax, %ax	/* 0 means success, else failure */
	jnz	boot1.halt16

	// print the done message
	push	$done_message
	call	real_puts
	add	$2, %sp

	// print the enter protected mode message
	push	$protected_message
	call	real_puts
	add	$2, %sp

	// next, load a provisory GDT, so we can enter protected mode.
	// To do that, interrupts need to be disabled
	cli			/* disable interrupts */
	lgdt	gdtr		/* load the GDT using the gdtr pointer */

	// now, enter protected mode. To do that, we grab the control
	// register 0 (%cr0) and set the protection enable bit
	mov	%cr0, %eax	/* grab %cr0 */
	or	$0x01, %al	/* set protection enable bit */
	mov	%eax, %cr0	/* push the new value to %cr0 */

	// all that is left to do is jump to the new protected mode
	// mark, using segment selector 0x08, as that selector describes
	// our code segment
	ljmp	$0x08, $boot1.protected

boot1.halt16:
	// print the halt message
	push	$halt_message
	call	real_puts
	add	$2, %sp

	// disable interrupts and halt
	cli
	hlt

	// repeat (We should not arrive here)
	jmp	boot1.halt16



.code32

boot1.protected:
	// -----------------------------
	// |   32 BIT PROTECTED MODE   |
	// -----------------------------

	// intitialize all segment registers again (using
	// the previously installed selectors)
	mov	$0x10, %eax	/* 0x10 is our data segment we defined */
	mov	%eax, %ds	/* data segment should be 0x10 */
	mov	%eax, %es	/* extra segment should be 0x10 */
	mov	%eax, %fs	/* general segment %fs should be 0x10 */
	mov	%eax, %gs	/* general segment %gs should be 0x10 */
	mov	%eax, %ss	/* stack segment should be 0x10 */

	// set the new stack and base pointers
	mov	$__boot1, %eax	/* take the boot1 linking address */
	mov	%eax, %ebp	/* assign that as the new base pointer */
	mov	%eax, %esp	/* assign that as the new stack pointer */

	// print the done message
	push	$done_message
	call	protected_puts
	add	$4, %esp

	// make sure paging is disabled
	mov	%cr0, %eax		/* grab %cr0 */
	and	$0x7fffffff, %eax	/* clear the paging enable bit */
	mov	%eax, %cr0		/* push the new value to %cr0 */

	// initialize the multiboot structure at
	// location 0x00000500
	push	$kernel_entry_addr	/* param: entry_addr */
	push	$kernel_load_amount	/* param: load_amount */
	push	$kernel_load_addr	/* param: load_addr */
	push	$kernel_memory_base	/* param: kernel */
	push	$mbi_memory_base	/* param: mbi */
	call	multiboot_handle
	add	$20, %esp
	test	%eax, %eax	/* 0 means success, else failure */
	jnz	boot1.halt32

	// copy the kernel to the correct location
	push	kernel_load_amount	/* param: len */
	push	$kernel_memory_base	/* param: src */
	push	kernel_load_addr	/* param: dest */
	call	copy_memory
	add	$12, %esp

	// finally, prepare all registers for the kernel,
	// and then jump to the specified entry address
	xor	%esi, %esi
	xor	%edi, %edi
	xor	%edx, %edx
	xor	%ecx, %ecx
	mov	$mbi_memory_base, %ebx
	mov	$0x36d76289, %eax
	push	kernel_entry_addr
	ret

boot1.halt32:
	// print the halt message
	push	$halt_message
	call	protected_puts
	add	$4, %sp

	// disable interrupts and halt
	cli
	hlt

	// repeat (We should not arrive here)
	jmp	boot1.halt32


.code32
.section .boot1.data, "aw", @progbits

.balign 4
.type gdtr, @object

gdtr:
	// --------------------
	// |   GDT REGISTER   |
	// --------------------

	// the GDT descriptor just describes where to find the actual GDT
	.word	gdt.end -gdt -1	/* 00..01: size of table minus one */
	.long	gdt		/* 02..05: linear address of the table */



.balign 4
.type gdt, @object

gdt:
	// -------------------------------
	// |   GLOBAL DESCRIPTOR TABLE   |
	// -------------------------------

	// we will create two GDT entries, one for the GDT itself,
	// and one for the data segment

	// first entry - null descriptor:
	.long	0x00000000	/* plain zero */
	.long	0x00000000	/* plain zero */

	// second entry - code descriptor:
	.word	0xffff		/* 00..01: limit bits 0..15 */
	.word	0x0000		/* 02..03: base bits 0..15 */
	.byte	0x00		/*     04: base bits 16..23 */
	.byte	0b10011010	/*     05: access byte */
	.byte	0b11001111	/*     06: limit bits 16..19 + flags */
	.byte	0x00		/*     07: base bits 24..31 */

	// third entry - data descriptor:
	.word	0xffff		/* 00..01: limit bits 0..15 */
	.word	0x0000		/* 02..03: base bits 0..15 */
	.byte	0x00		/*     04: base bits 16..23 */
	.byte	0b10010010	/*     05: access byte */
	.byte	0b11001111	/*     06: limit bits 16..19 + flags */
	.byte	0x00		/*     07: base bits 24..31 */

gdt.end:



kernel_load_addr:
	// the physical memory address where the kernel
	// should be loaded to
	.long	0x00000000
kernel_load_amount:
	// the amount of bytes of the kernel binary that
	// should be loaded
	.long	0x00000000
kernel_entry_addr:
	// the physical memory address the bootloader should
	// jump to after loading it to the correct location
	.long	0x00000000



kernel_file:
	// file name of the kernel binary to search for
	.string "boot/kernel.bin"

done_message:
	// message indicating the execution ended successfully
	.string	"Done.\n"

halt_message:
	// message indicating that the system halted
	.string	"[SYSTEM HALTED]"

a20_message:
	// message indicating that the A20 gate will be enabled
	.string "Enabling A20 gate... "

lower_memory_message:
	// message indicating that the lower memory bounds
	// are beeing determined
	.string "Determining lower memory bounds... "

upper_memory_message:
	// message indicating that the upper memory bounds
	// are beeing determined
	.string "Determining extended memory bounds... "

mmap_message:
	// message indicating that the MMAP structure is beeing
	// loaded into memory
	.string "Loading the MMAP structure into memory... "

kernel_read_message:
	// message indicating that the loader is searching the
	// kernel binary in the current filesystem
	.string "Reading the kernel binary into memory... "

protected_message:
	// message indicating that protected mode will now
	// be activated
	.string	"Entering 32bit protected mode... "

