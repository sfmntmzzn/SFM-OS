
/* ----------------------------------------------------------------- *
 *
 *   string_length16.s: get the length of a NUL-terminated string
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.code16
.org 0x00

.section .boot1, "awx", @progbits

.global string_length16
.type string_length16, @function

string_length16:
	// -------------------------------
	// |   STRING_LENGTH16(string)   |
	// -------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original value of %cx and %di
	push	%cx
	push	%di

	// set %di to point to the string,
	// set %ax to 0x00 and %cx to 0xFFFF
	mov	4(%bp), %di
	mov	$0x00, %ax
	mov	$0xffff, %cx

	// clear the direction flag, just to be sure
	cld

	// count the length of the passed filename
	repne	scasb

	// negate %cx and subtract 2
	neg	%cx
	sub	$2, %cx

	// move the value in %cx into %ax, as %ax will hold the
	// return value
	mov	%cx, %ax

	// restore the original value of %cx and %di
	pop	%di
	pop	%cx

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

