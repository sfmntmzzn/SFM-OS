
/* ----------------------------------------------------------------- *
 *
 *   a20.s: enable the A20 gate using different attempts
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern real_puts

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global a20
.type a20, @function

a20:
	// -------------
	// |   A20()   |
	// -------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// retrieve the current eflags value, and then
	// disable all maskable interrupts
	pushf
	cli

	// check if A20 gate is somehow enabled already
	call	__a20_check
	test	%ax, %ax
	jnz	a20.done

	// try the BIOS method to activate the A20 gate
	call	__bios_method

	// check if A20 is enabled now
	call	__a20_check
	test	%ax, %ax
	jnz	a20.done

	// try the fast A20 method to activate the A20 gate
	call	__fast_method

	// check if A20 is enabled now
	call	__a20_check
	test	%ax, %ax
	jnz	a20.done

	// try the keyboard method to activate the A20 gate
	call	__keyboard_method

	// check if A20 is enabled now
	call	__a20_check
	test	%ax, %ax
	jnz	a20.done

a20.fail:
	// set %ax to 1
	mov	$1, %ax

	// jump to the end label
	jmp	a20.next

a20.done:
	// wipe the return value in %ax
	xor	%ax, %ax

a20.next:
	// pop the eflags value from the stack
	pop	%dx

	// test if the interrupts flag is set
	and	$(1<<9), %dx
	jz	a20.end

	// reenable interrupts
	sti

a20.end:
	// restore the old base pointer from stack, and return
	pop	%bp
	ret



.type __a20_check, @function

__a20_check:
	// ---------------------
	// |   __A20_CHECK()   |
	// ---------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// push all registers we want to use to the stack to preserve their
	// current state
	push	%es
	push	%ds
	push	%di
	push	%si

	// set %es to 0x0000 and %di to 0x0500
	xor	%ax, %ax
	mov	%ax, %es
	mov	$0x0500, %di

	// set %ds tp 0xFFFF and %si to 0x0510
	not	%ax
	mov	%ax, %ds
	mov	$0x0510, %si

	// save the values that are currently at those locations
	movb	%es:(%di), %al
	movb	%ds:(%si), %ah
	push	%ax

	// set %es:%di to 0x00 and %ds:%si to 0xFF
	movb	$0x00, %es:(%di)
	movb	$0xff, %ds:(%si)

	// if %es:%di is now 0xFF, then the A20 gate is disabled, as the
	// memory has been wrapped
	cmpb	$0xff, %es:(%di)
	je	__a20_check.negative

	// if memory has not been wrapped, the A20 is enabled
	mov	$0x0001, %ax

__a20_check.end:
	// restore the previous values from the memory locations
	pop	%dx
	movb	%dh, %ds:(%si)
	movb	%dl, %es:(%di)

	// restore the registers that we saved to the stack
	pop	%si
	pop	%di
	pop	%ds
	pop	%es

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

__a20_check.negative:
	// set %ax to zero
	xor	%ax, %ax

	// jump back up
	jmp	__a20_check.end



.type __bios_method, @function

__bios_method:
	// -----------------------
	// |   __BIOS_METHOD()   |
	// -----------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// temporarily enable interrupts
	sti

	// call BIOS interrupt 0x15 with AX=0x2401
	mov	$0x2401, %ax
	int	$0x15

	// redisable interrupts
	cli

	// restore the old base pointer from stack, and return
	pop	%bp
	ret



.type __fast_method, @function

__fast_method:
	// -----------------------
	// |   __FAST_METHOD()   |
	// -----------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// copy the value from port 0x92 into %al
	inb	$0x92, %al

	// set the A20 enable bit, and reput that value in port 0x92
	orb	$0x02, %al
	outb	%al, $0x92

	// restore the old base pointer from stack, and return
	pop	%bp
	ret



.type __keyboard_method, @function

__keyboard_method:
	// ---------------------------
	// |   __KEYBOARD_METHOD()   |
	// ---------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// wait until the keyboard is ready
	call	__keyboard_method.wait1

	// disable the keyboard
	mov	$0xad, %al
	outb	%al, $0x64
	call	__keyboard_method.wait1

	// set method: read from keyboard input
	mov	$0xd0, %al
	outb	%al, $0x64
	call	__keyboard_method.wait2

	// get the keyboard data
	inb	$0x60, %al
	push	%ax
	call	__keyboard_method.wait1

	// set method: write to keyboard output
	mov	$0xd1, %al
	outb	%al, $0x64
	call	__keyboard_method.wait1

	// send the collected data
	pop	%ax
	orb	$0x02, %al
	outb	%al, $0x60
	call	__keyboard_method.wait1

	// finally, reenable the keyboard
	mov	$0xae, %al
	outb	%al, $0x64
	call	__keyboard_method.wait1

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

__keyboard_method.wait1:
	// copy the value from port 0x64 into %al
	inb	$0x64, %al

	// as long as %al is not 2, loop
	test	$0x02, %al
	jnz	__keyboard_method.wait1

	// return
	ret

__keyboard_method.wait2:
	// copy the value from port 0x64 into %al
	inb	$0x64, %al

	// as long as %al is not 1, loop
	test	$0x01, %al
	jnz	__keyboard_method.wait2

	// return
	ret

