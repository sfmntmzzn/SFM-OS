
/* ----------------------------------------------------------------- *
 *
 *   text.s: contains some useful variables for the vga text
 *           mode output.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.code32
.org 0x00

.section .boot1.data, "aw", @progbits

.global text_cursor_location
.type text_cursor_location, @object

	.balign 2
text_cursor_location:
	// ---------------------------
	// |   TEXT CURSOR LOCATION   |
	// ---------------------------

	// the text cursor location stores the vga cursor position. The
	// vga cursor position indicates the location the next character
	// will be printed to. Getting the next cursor location may depend
	// on the current video mode.

	// to get the actual coordinates out of this value, you have to
	// perform this calculations:
	// Y := floor( %value% div %video_mode_width% )
	// X := %value% mod %video_mode_width%

	.word	0x0000

