
/* ----------------------------------------------------------------- *
 *
 *   real_putc.s: print a character to the screen while in
 *                real mode
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */

.include "text.inc"

	.set	text_memory_segment, ( text_memory_base >> 4 ) & 0xFFFF

.extern text_cursor_location

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global real_putc
.type real_putc, @function

real_putc:
	// -------------------
	// |   REAL_PUTC()   |
	// -------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original values of %ebx and %es
	pushl	%ebx
	push	%es

	// push the current EFLAGS state to the stack
	pushf

	// disable interrupts
	cli

	// load the current cursor position into %bx
	xorl	%ebx, %ebx
	movw	text_cursor_location, %bx

	// load the text memory segment into %es
	push	$text_memory_segment
	pop	%es

	// load the character to print into %ax
	movb	4(%bp), %al

	// check if the character we have to print is below 0x20
	cmpb	$0x20, %al
	jb	real_putc.special

real_putc.paste:
	// write that character to the vga text memory
	movb	%al, %es:0(,%ebx,2)
	movb	$0x07, %es:1(,%ebx,2)

	// increase the current cursor position, and store the new value
	// back to memory
	inc	%bx
	movw	%bx, text_cursor_location

	// jump to the end of the function
	jmp	real_putc.end

real_putc.special:
	// check if we have a \n character
	cmpb	$'\n', %al
	je	real_putc.special.n

	// check if we have a \r character
	cmpb	$'\r', %al
	je	real_putc.special.r

	// the character wasn't one of these, so just jump to the end
	jmp	real_putc.end

real_putc.special.n:
	// increase the current cursor position by `columns'
	add	$text_mode_columns, %bx

	// store the new cursor position
	movw	%bx, text_cursor_location

real_putc.special.r:
	// move the current cursor position into %ax
	mov	%bx, %ax

	// clear %dx
	xor	%dx, %dx

	// load `columns' into %cx
	push	%cx
	movw	$text_mode_columns, %cx

	// get the modulo of the current cursor position
	div	%cx
	pop	%cx

	// subtract the remainder and store the new cursor position
	sub	%dx, %bx
	movw	%bx, text_cursor_location

	// jump to the end of this routine
	jmp	real_putc.end		

real_putc.end:
	// tell we want to transfer the high byte first
	mov	$0x0e, %al
	mov	$0x03d4, %dx
	outb	%al, %dx

	// transfer the high byte
	mov	%bx, %ax
	shr	$8, %ax
	mov	$0x03d5, %dx
	outb	%al, %dx

	// tell we want to transfer the low byte next
	mov	$0x0f, %al
	mov	$0x03d4, %dx
	outb	%al, %dx

	// transfer the low byte
	mov	%bx, %ax
	mov	$0x03d5, %dx
	outb	%al, %dx

	// pop the EFLAGS into %ax, and check whether
	// the interrupt bit was set
	pop	%ax
	andb	$0x02, %ah
	jz	real_putc.next

	// reenable interrupts
	sti

real_putc.next:
	// restore the original values of %ebx and %es
	pop	%es
	popl	%ebx

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

