
/* ----------------------------------------------------------------- *
 *
 *   read_sector.s: read a specific sector from disk into memory
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern real_putc
.extern real_puts

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global read_sectors
.type read_sectors, @function

read_sectors:
	// -------------------------------------------------------
	// |   READ_SECTORS(drive,sector,count,segment,offset)   |
	// -------------------------------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the base pointer to the current stack pointer
	mov	%sp, %bp

	// store the original value of %bx, %cx, %di and %si
	push	%bx
	push	%cx
	push	%di
	push	%si

	// create space for the bootsector to be loaded and
	// let %di point to this space
	sub	$512, %sp
	mov	%sp, %di

	// read the drives boot sector from disk
	push	%di		/* param: offset */
	push	%ss		/* param: segment */
	push	$1		/* param: sector */
	push	$0		/* param: cylinder */
	push	$0		/* param: head */
	push	4(%bp)		/* param: device */
	call	read_sector
	add	$12, %sp

	// check the return value
	test	%ax, %ax
	jnz	read_sectors.fail

	// attach a dot to the message
	pushw	$'.'
	call	real_putc
	add	$2, %sp

	// set %si to the start sector and %cx to the address
	mov	6(%bp), %si
	mov	12(%bp), %cx

	// set %bx to the first sector not to be loaded
	mov	8(%bp), %bx
	add	%si, %bx

read_sectors.loop:
	// check if we arrived at the last sector
	cmp	%si, %bx
	je	read_sectors.end

	// push %bx, we need that register
	push	%bx

	// read a single sector from the disk, in order to do that,
	// we have to convert the passed LBA sector address to an CHS address
	push	%cx		/* param: offset */
	push	10(%bp)		/* param: segment */
	mov	%si, %ax
	xor	%dx, %dx
	mov	24(%di), %bx
	div	%bx
	add	$0x01, %dx
	push	%dx		/* param: sector */
	xor	%dx, %dx
	mov	26(%di), %bx
	div	%bx
	push	%ax		/* param: cylinder */
	push	%dx		/* param: head */
	push	4(%bp)		/* param: device */
	call	read_sector
	add	$12, %sp

	// pop %bx, we don't need it anymore
	pop	%bx

	// check the return value
	test	%ax, %ax
	jnz	read_sectors.fail

	// attach a dot to the message
	pushw	$'.'
	call	real_putc
	add	$2, %sp

	// increase %si by 1 and %cx by 512
	add	$1, %si
	add	$512, %cx

	// jump to the next iteration
	jmp	read_sectors.loop

read_sectors.end:
	// pop drive boot sector from stack
	add	$512, %sp

	// restore the original value of %bx, %cx, %di and %si
	pop	%si
	pop	%di
	pop	%cx
	pop	%bx

	// set the return value to 0x00
	xor	%ax, %ax

	// restore the original base pointer, and return
	pop	%bp
	ret	

read_sectors.fail:
	// pop drive boot sector from stack
	add	$512, %sp

	// restore the original value of %bx, %cx, %di and %si
	pop	%si
	pop	%di
	pop	%cx
	pop	%bx

	// set the return value to 0x01
	mov	$0x0001, %ax

	// restore the original base pointer, and return
	pop	%bp
	ret



read_sector:
	// --------------------------------------------------------------
	// |   READ_SECTOR(drive,head,cylinder,sector,segment,offset)   |
	// --------------------------------------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the base pointer to the current stack pointer
	mov	%sp, %bp

	// push the original value of the used registers to the stack
	push	%bx
	push	%cx
	push	%di

	// clean %ax, %bx, %cx and %dx
	xor	%ax, %ax
	xor	%bx, %bx
	xor	%cx, %cx
	xor	%dx, %dx

	// for the next operation we will need to have interrupts
	// disabled, as we will be messing around with segment registers
	cli

	// push the extra segment register to the stack to save its value
	push	%es
	mov	12(%bp), %es

	// in case we have interrupts disabled, enable them now
	sti

	// use %di as our try counter. We will grant 5 retries before
	// returning with an error
	mov	$5, %di

read_sector.try:
	// prepare all registers for a reset
	movb	$0x00, %ah
	movb	4(%bp), %dl

	// clear the carry flag, as it will indicated if the
	// interrupt was successful
	clc

	// call the BIOS interrupt routine to reset the disk
	int	$0x13

	// if the carry flag is set, something went wrong
	jc	read_sector.error

	#prepare all registers for the read of the sector
	movb	$0x02, %ah
	movb	$0x01, %al
	movb	4(%bp), %dl
	movb	6(%bp), %dh
	movb	8(%bp), %ch
	movb	10(%bp), %cl
	mov	14(%bp), %bx

	// clear the carry flag, as it will indicated if the
	// interrupt was successful
	clc

	// call the BIOS interrupt routine to read the sector
	int	$0x13

	// if the carry flag is set, something went wrong
	jc	read_sector.error

	// for the next operation we will need to have interrupts
	// disabled, as we will be messing around with segment registers
	cli

	// restore the original value of the extra segment register
	pop	%es

	// in case we have interrupts disabled, enable them now
	sti

	// restore the original value of the used registers
	pop	%di
	pop	%cx
	pop	%bx

	// wipe the return value in %ax
	xor	%ax, %ax

	// restore the original base pointer, and return
	pop	%bp
	ret

read_sector.error:
	// if %di has arrived to 0, we have no retries left, and
	// therefore have to return an error
	test	%di, %di
	jz	read_sector.fail

	// otherwise, decrease the counter by one
	dec	%di

	// and jump back to the next loop iteration
	jmp	read_sector.try

read_sector.fail:
	// for the next operation we will need to have interrupts
	// disabled, as we will be messing around with segment registers
	cli

	// restore the original value of the extra segment register
	pop	%es

	// in case we have interrupts disabled, enable them now
	sti

	// restore the original value of the used registers
	pop	%di
	pop	%cx
	pop	%bx

	// set the return value to 0x01
	mov	$0x0001, %ax

	// restore the original base pointer, and return
	pop	%bp
	ret

