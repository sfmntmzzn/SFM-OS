
/* ----------------------------------------------------------------- *
 *
 *   real_puts.s: print a NUL terminated string to the screen
 *                while in real mode
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */

.extern real_putc

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global real_puts
.type real_puts, @function

real_puts:
	// -------------------
	// |   REAL_PUTS()   |
	// -------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original value of the %si register
	push	%si

	// take the char pointer from the stack into %si
	movw	4(%bp), %si

real_puts.loop:
	// load the byte %si points to into %ax, and increase %si to
	// point to the following byte
	movb	(%si), %al
	inc	%si

	// if we arrived to the NUL byte, we can stop looping
	and	$0x00FF, %ax
	jz	real_puts.end

	// push %ax and call `putc'
	push	%ax
	call	real_putc
	add	$2, %sp

	// jump back to the next loop iteration
	jmp	real_puts.loop

real_puts.end:
	// restore the original value of %si
	pop	%si

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

