
/* ----------------------------------------------------------------- *
 *
 *   fat12_filename.s: convert the given filename to a
 *                     FAT12 compatible filename
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.code16
.org 0x00

.section .boot1, "awx", @progbits

.global fat12_filename
.type fat12_filename, @function

fat12_filename:
	// ---------------------------------------
	// |   FAT12_FILENAME(filename,buffer)   |
	// ---------------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original values of %si and %di
	push	%si
	push	%di

	// set %si to point to the filename, %di to point to the buffer
	// and %dx to the characters left to print
	mov	4(%bp), %si
	mov	6(%bp), %di
	mov	$8, %dx

fat12_filename.name.loop:
	// check whether there are characters left to copy
	test	%dx, %dx
	jz	fat12_filename.name.pad

	// load the byte %si points to into %al
	movb	(%si), %al

	// if the character is below '0', skip the character
	cmpb	$'0', %al
	jb	fat12_filename.name.loop.next

	// if the character is below-equal '9', copy the character
	cmpb	$'9', %al
	jbe	fat12_filename.name.loop.copy

	// if the character is below 'A', skip the character
	cmpb	$'A', %al
	jb	fat12_filename.name.loop.next

	// if the character is below-equal 'Z', copy the character
	cmpb	$'Z', %al
	jbe	fat12_filename.name.loop.copy

	// if the character is below 'a' or above 'z', skip the character
	cmpb	$'a', %al
	jb	fat12_filename.name.loop.next
	cmpb	$'z', %al
	ja	fat12_filename.name.loop.next

	// subtract 32 from %al, so lowercase letters become uppercase
	subb	$0x20, %al

fat12_filename.name.loop.copy:
	// copy the current buffer into the buffer,
	// and increase %di
	movb	%al, (%di)
	inc	%di

	// decrease the characters left counter by 1
	dec	%dx

fat12_filename.name.loop.next:
	// check if we have the NUL byte. If so, move on filling
	// space character into the new buffer
	testb	%al, %al
	jz	fat12_filename.name.fill

	// next, do the same check for '.'
	cmpb	$'.', %al
	je	fat12_filename.name.fill

	// otherwise, increase %si and move on with the
	// next loop iteration
	inc	%si
	jmp	fat12_filename.name.loop

fat12_filename.name.fill:
	// check whether there are characters left to copy
	test	%dx, %dx
	jz	fat12_filename.name.end

	// copy the space character to the buffer
	movb	$' ', (%di)
	inc	%di

	// decrease the characters left counter
	dec	%dx

	// move on with the next loop iteration
	jmp	fat12_filename.name.fill

fat12_filename.name.pad:
	// load the byte %si points to into %al
	movb	(%si), %al

	// check if we have the NUL byte. If so, move on filling
	// space character into the new buffer
	testb	%al, %al
	jz	fat12_filename.name.end

	// next, do the same check for '.'
	cmpb	$'.', %al
	je	fat12_filename.name.end

	// otherwise, increase %si and move on with the
	// next loop iteration
	inc	%si
	jmp	fat12_filename.name.pad

fat12_filename.name.end:
	// 3 characters left for the filename extension
	mov	$3, %dx

	// load the byte %si points to into %al
	movb	(%si), %al

	// check if we have the NUL byte. If so, move on filling
	// space character into the new buffer
	testb	%al, %al
	jz	fat12_filename.ext.fill

	// increase %si, as we dont need the '.' character
	inc	%si

fat12_filename.ext.loop:
	// check whether there are characters left to copy
	test	%dx, %dx
	jz	fat12_filename.end

	// load the byte %si points to into %al
	movb	(%si), %al

	// if the character is below '0', skip the character
	cmpb	$'0', %al
	jb	fat12_filename.ext.loop.next

	// if the character is below-equal '9', copy the character
	cmpb	$'9', %al
	jbe	fat12_filename.ext.loop.copy

	// if the character is below 'A', skip the character
	cmpb	$'A', %al
	jb	fat12_filename.ext.loop.next

	// if the character is below-equal 'Z', copy the character
	cmpb	$'Z', %al
	jbe	fat12_filename.ext.loop.copy

	// if the character is below 'a' or above 'z', skip the character
	cmpb	$'a', %al
	jb	fat12_filename.ext.loop.next
	cmpb	$'z', %al
	ja	fat12_filename.ext.loop.next

	// subtract 32 from %al, so lowercase letters become uppercase
	subb	$0x20, %al

fat12_filename.ext.loop.copy:
	// copy the current buffer into the buffer,
	// and increase %di
	movb	%al, (%di)
	inc	%di

	// decrease the characters left counter by 1
	dec	%dx

fat12_filename.ext.loop.next:
	// check if we have the NUL byte. If so, move on filling
	// space character into the new buffer
	testb	%al, %al
	jz	fat12_filename.ext.fill

	// otherwise, increase %si and move on with the
	// next loop iteration
	inc	%si
	jmp	fat12_filename.ext.loop

fat12_filename.ext.fill:
	// check whether there are characters left to copy
	test	%dx, %dx
	jz	fat12_filename.end

	// copy the space character to the buffer
	movb	$' ', (%di)
	inc	%di

	// decrease the characters left counter
	dec	%dx

	// move on with the next loop iteration
	jmp	fat12_filename.ext.fill

fat12_filename.end:
	// restore the original values of %si and %di
	pop	%di
	pop	%si

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

