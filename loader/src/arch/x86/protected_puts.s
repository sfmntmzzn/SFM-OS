
/* ----------------------------------------------------------------- *
 *
 *   protected_puts.s: print a NUL terminated string to the screen
 *                     while in protected mode
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */

.extern protected_putc

.code32
.org 0x00

.section .boot1, "awx", @progbits

.global protected_puts
.type protected_puts, @function

protected_puts:
	// ------------------------
	// |   PROTECTED_PUTS()   |
	// ------------------------

	// push the old base pointer to the stack
	push	%ebp

	// set the new base pointer to the current stack pointer
	mov	%esp, %ebp

	// save the original value of the %eax and %esi registers
	push	%eax
	push	%esi

	// take the char pointer from the stack into %esi
	mov	8(%ebp), %esi

protected_puts.loop:
	// load the next byte %esi points to into %eax, and increase %esi
	// to point to the following byte
	xor	%eax, %eax
	movb	(%esi), %al
	inc	%esi

	// if we arrived to the NUL byte, we can stop looping
	cmp	$0x00000000, %eax
	je	protected_puts.end

	// push %ax and call putc
	push	%eax
	call	protected_putc
	add	$4, %esp

	// jump back to the next loop iteration
	jmp	protected_puts.loop

protected_puts.end:
	// restore the original value of %eax and %esi
	pop	%esi
	pop	%eax

	// restore the old base pointer from stack, and return
	pop	%ebp
	ret

