
/* ----------------------------------------------------------------- *
 *
 *   protected_putc.s: print a character to the screen while in
 *                     32bit protected mode.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */

.include "text.inc"

.extern text_cursor_location

.code32
.org 0x00

.section .boot1, "awx", @progbits

.global protected_putc
.type protected_putc, @function

protected_putc:
	// ------------------------
	// |   PROTECTED_PUTC()   |
	// ------------------------

	// push the old base pointer to the stack
	push	%ebp

	// set the new base pointer to the current stack pointer
	mov	%esp, %ebp

	// save the original value of %eax, %ebx and %edx
	push	%eax
	push	%ebx
	push	%edx

	// load the current cursor position into %ebx
	xor	%ebx, %ebx
	movw	text_cursor_location, %bx

	// load the character to print into %eax
	movb	8(%ebp), %al

	// check if the character we have to print is below 0x20
	cmpb	$0x20, %al
	jb	protected_putc.special

protected_putc.paste:
	// load the text memory base address into %edx
	mov	$text_memory_base, %edx

	// write that character to the vga text memory
	movb	%al, 0(%edx,%ebx,2)
	movb	$0x07, 1(%edx,%ebx,2)

	// increase the current cursor position, and store the
	// new value back to its memory location
	inc	%ebx
	movw	%bx, text_cursor_location

	// jump to the next step in this routine
	jmp	protected_putc.check

protected_putc.special:
	// check if we have a \n character
	cmpb	$'\n', %al
	je	protected_putc.special.n

	// check if we have a \r character
	cmpb	$'\r', %al
	je	protected_putc.special.r

	// the character wasn't one of these, so just move on
	jmp	protected_putc.check

protected_putc.special.n:
	// increase the current cursor position by `columns'
	add	$text_mode_columns, %ebx

	// store the new cursor position
	movw	%bx, text_cursor_location

	// jump to the next step in this routine
	jmp	protected_putc.check

protected_putc.special.r:
	// move the current cursor position into %eax
	mov	%ebx, %eax

	// clear %edx
	xor	%edx, %edx

	// load `columns' into %ecx
	push	%ecx
	xor	%ecx, %ecx
	movw	$text_mode_columns, %cx

	// get the modulo of the current cursor position
	div	%ecx
	pop	%ecx

	// subtract the remainder and store the new cursor position
	sub	%edx, %ebx
	movw	%bx, text_cursor_location		

protected_putc.check:
	// load the current cursor position into %ebx
	xor	%ebx, %ebx
	movw	text_cursor_location, %bx

	// store the current values of %ecx and %edx on the stack
	push	%ecx
	push	%edx

	// move `columns*rows' into %eax
	mov	$text_mode_columns, %eax
	mov	$text_mode_rows, %ecx
	mul	%ecx

protected_putc.check.loop:
	// check whether the current cursor position exceeds
	// that calculated product, i.e. would be outside of
	// the screen
	cmp	%eax, %ebx
	jb	protected_putc.check.end

	// push the current values of %eax and %ebx to the stack
	push	%eax
	push	%ebx

	// let %eax point to the first character in the last row
	// and %acx to the first character after the first row
	mov	%eax, %ecx
	sub	$text_mode_columns, %ecx
	xor	%eax, %eax

	// let %ebx point to `text_memory_base'
	mov	$text_memory_base, %ebx	

protected_putc.move:
	// check wether we had arrived at the last byte
	cmp	%ecx, %eax
	jae	protected_putc.move.end

	// move the character from the row above to the
	// one %eax points to
	add	$text_mode_columns, %eax
	movw	0(%ebx,%eax,2), %dx
	sub	$text_mode_columns, %eax
	movw	%dx, 0(%ebx,%eax,2)

	// increase %eax, and move on with the next loop iteration
	inc	%eax
	jmp	protected_putc.move

protected_putc.move.end:
	// restore the value of %eax into %ecx
	mov	4(%esp), %ecx

protected_putc.clear:
	// check wether we had arrived at the last byte
	cmp	%ecx, %eax
	jae	protected_putc.clear.end

	// clear the character %eax points to
	movw	$0x0700, 0(%ebx,%eax,2)

	// increase %eax, and move on with the next loop iteration
	inc	%eax
	jmp	protected_putc.clear

protected_putc.clear.end:
	// restore the values of %eax and %ebx
	pop	%ebx
	pop	%eax

	// decrease %ebx by `columns'
	sub	$text_mode_columns, %ebx

	// move on with another iteration of the loop
	jmp	protected_putc.check.loop

protected_putc.check.end:
	// update the current cursor position
	movw	%bx, text_cursor_location

	// pop the stored values of %ebx, %ecx and %edx
	pop	%edx
	pop	%ecx

protected_putc.end:
	// tell we want to transfer the high byte first
	mov	$0x0e, %al
	mov	$0x03d4, %dx
	out	%al, %dx

	// transfer the high byte
	mov	%bx, %ax
	shr	$8, %ax
	mov	$0x03d5, %dx
	out	%al, %dx

	// tell we want to transfer the low byte next
	mov	$0x0f, %al
	mov	$0x03d4, %dx
	out	%al, %dx

	// transfer the low byte
	mov	%bx, %ax
	mov	$0x03d5, %dx
	out	%al, %dx

	// restore the original value of %eax, %ebx and %edx
	pop	%edx
	pop	%ebx
	pop	%eax

	// restore the old base pointer from stack, and return
	pop	%ebp
	ret

