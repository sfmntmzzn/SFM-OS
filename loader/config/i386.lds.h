
//
//
//   i386.lds.h: linker script receipt for a i386 target.
//
//   Copyright (C) 2018 Sandro Francesco Montemezzani
//   Author: Sandro Montemezzani <sandro@montemezzani.net>
//
//   This file is part of the bootloader for the SFM-OS kernel,
//   and is licensed under the MIT License.
//
//


#undef i386

OUTPUT_FORMAT("elf32-i386", "elf32-i386", "elf32-i386")
OUTPUT_ARCH(i386)
ENTRY(_start)


#define	SECTOR_SIZE		0x200
#define TEXT_BASE		0x7c00
#define LOADER_IMAGE_SIZE	1<<32


MEMORY
{
    bootsector(rwx): ORIGIN = TEXT_BASE,
                     LENGTH = SECTOR_SIZE
    mem(rwx): ORIGIN = TEXT_BASE + SECTOR_SIZE,
              LENGTH = LOADER_IMAGE_SIZE
}


SECTIONS
{
    PROVIDE(__loader_start = .);

    .boot0 : AT(0x00)
    {
        PROVIDE(__boot0 = .);
        PROVIDE(__boot0_start = .);

        *(.boot0*)

        PROVIDE(__boot0_end = .);
    } > bootsector

    .boot1 : AT( LOADADDR(.boot0) + SIZEOF(.boot0) )
    {
        PROVIDE(__boot1 = .);
        PROVIDE(__boot1_start = .);

        *(.boot1*)
        *(.text)
        *(.rodata*)
        *(.data)
        *(.bss)

        PROVIDE(__boot1_end = .);
    } > mem

    PROVIDE(__loader_end = .);

    /DISCARD/ : 
    {
        *(.comment)
    }
}

