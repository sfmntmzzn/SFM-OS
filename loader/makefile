
#
#
#   makefile: the loader makefile that powers the GNU make
#             build system for the bootloader compilation.
#
#   Copyright (C) 2018 Sandro Francesco Montemezzani
#   Author: Sandro Montemezzani <sandro@montemezzani.net>
#
#   This file is part of the SFM-OS project, and is licensed
#   under the MIT License.
#
#


#################
#  BASIC SETUP  #
#################

PROJECT := sfmos-loader


#########################
#  DIRECTORY VARIABLES  #
#########################

bindir     := $(CURDIR)/bin
builddir   := $(CURDIR)/build
configdir  := $(CURDIR)/config
includedir := $(CURDIR)/include
scriptsdir := $(CURDIR)/scripts
srcdir     := $(CURDIR)/src


#################
#  FILES SETUP  #
#################

VERSION_HEADER           := $(includedir)/version.h
VERSION_HEADER_TEMPLATE  := $(srcdir)/version.h.in
VERSION_HEADER_GENERATOR := $(scriptsdir)/make_version.sh

LINKER_SCRIPT_RECEIPT := $(configdir)/generic.lds.h
LINKER_SCRIPT         := $(builddir)/link.lds

LOADER_BINARY := $(bindir)/$(PROJECT).bin
LOADER_FILE   := $(bindir)/$(PROJECT).sys


##################
#  BUILD CONFIG  #
##################

LOADER_BUILD_CONFIG_FAT_SUPPORT ?= 1


#############################
#  UPDATE COMPILER OPTIONS  #
#############################

CROSS_ASFLAGS  += -I$(includedir)
CROSS_CFLAGS   += -Os -ffreestanding -nostdlib -nostdinc \
                  -fno-strict-aliasing -fno-common -fomit-frame-pointer
CROSS_CPPFLAGS := -I$(includedir)
CROSS_LDFLAGS  += -T$(LINKER_SCRIPT) -static -lgcc

ifeq ($(LOADER_BUILD_CONFIG_FAT_SUPPORT),1)
  CROSS_CPPFLAGS += -DBUILD_CONFIG_FAT_SUPPORT
endif


#######################
#  COMPILATION UNITS  #
#######################

OBJECT_FILES := \
	$(builddir)/obj/copy_memory.o \
	$(builddir)/obj/multiboot.o \
	$(builddir)/obj/string_length.o \
	$(builddir)/obj/version.o


#################
#  FINAL SETUP  #
#################

# include the architecture specific configuration
-include $(configdir)/$(ARCH).mk

# create the `cmd' variable
cmd = @$(if $($(quiet)cmd_$(1)),echo "$($(quiet)cmd_$(1))" &&) $(cmd_$(1))


##########################
#  ALL - DEFAULT TARGET  #
##########################

PHONY := all
all: loader


###################
#  CLEAN TARGETS  #
###################

qcmd_rmfile = RM $(2)
cmd_rmfile  = rm -f $(2)

qcmd_rmdir = RM $(2)
cmd_rmdir  = rm -rf $(2)

qcmd_rmcontents = RM $(2)/*
cmd_rmcontents  = rm -rf $(2)/*

PHONY += clean
clean:
	+$(call cmd,rmcontents,$(bindir))
	+$(call cmd,rmcontents,$(builddir))
	+$(call cmd,rmfile,$(VERSION_HEADER))

PHONY += distclean
distclean:
	+$(call cmd,rmdir,$(bindir))
	+$(call cmd,rmdir,$(builddir))
	+$(call cmd,rmfile,$(VERSION_HEADER))


####################
#  VERSION HEADER  #
####################

qcmd_make_version = GEN $@
cmd_make_version  = $(VERSION_HEADER_GENERATOR) $< $@

$(VERSION_HEADER): $(VERSION_HEADER_TEMPLATE) FORCE
	+$(call cmd,make_version)

$(VERSION_HEADER): | $(dir $(VERSION_HEADER))


#########################
#  COMPILATION TARGETS  #
#########################

qcmd_compile_assembly = AS $@
cmd_compile_assembly  = $(CROSS_CC) $(CROSS_CFLAGS) $(CROSS_CPPFLAGS) \
                        -c -o $@ -MMD -MP -MF $(builddir)/obj/$*.Td \
                        -xassembler-with-cpp $< \
                        $(CROSS_ASFLAGS:%=-Xassembler %)

qcmd_compile_c = CC $@
cmd_compile_c  = $(CROSS_CC) $(CROSS_CFLAGS) $(CROSS_CPPFLAGS) \
                 -c -o $@ -MMD -MP -MF $(builddir)/obj/$*.Td \
                 -xc $<

cmd_copy_depfile = @mv -f $(builddir)/obj/$*.Td $(builddir)/obj/$*.d

$(builddir)/obj/%.o: $(srcdir)/%.s | $(VERSION_HEADER)
	+$(call cmd,compile_assembly)
	+$(call cmd_copy_depfile)

$(builddir)/obj/%.o: $(srcdir)/%.c | $(VERSION_HEADER)
	+$(call cmd,compile_c)
	+$(call cmd_copy_depfile)

.SECONDEXPANSION:
$(OBJECT_FILES): | $$(dir $$@)

-include $(OBJECT_FILES:%.o=%.d)


###################
#  LINKER SCRIPT  #
###################

qcmd_mk_lds = GEN $@
cmd_mk_lds  = $(CROSS_CPP) $(CROSS_CPPFLAGS) $< | grep -v '^\#' > $@

$(LINKER_SCRIPT): $(LINKER_SCRIPT_RECEIPT) FORCE | $(dir $(LINKER_SCRIPT))
	+$(call cmd,mk_lds)


###################
#  LOADER TARGET  #
###################

qcmd_link_loader = LD $@
cmd_link_loader  = $(CROSS_CC) $(CROSS_CFLAGS) $(CROSS_CPPFLAGS) \
                   -o $@ $^ $(CROSS_LDFLAGS:%=-Xlinker %)

PHONY += loader
loader: $(LOADER_FILE)

$(LOADER_FILE): $(OBJECT_FILES) | $(LINKER_SCRIPT) $(dir $(LOADER_FILE))
	+$(call cmd,link_loader)

export LOADER_FILE


###################
#  BINARY TARGET  #
###################

qcmd_strip_loader = STRIP $@
cmd_strip_loader  = $(CROSS_OBJCOPY) -O binary $< $@

PHONY += binary
binary: $(LOADER_BINARY)

$(LOADER_BINARY): $(LOADER_FILE) | $(dir $(LOADER_BINARY))
	+$(call cmd,strip_loader)

export LOADER_BINARY


#######################
#  DIRECTORY TARGETS  #
#######################

qcmd_mkdir = MKDIR $@
cmd_mkdir  = mkdir -p $@

$(sort $(PREFIX)/bin $(dir \
		$(VERSION_HEADER) \
		$(OBJECT_FILES) \
		$(LINKER_SCRIPT) \
		$(LOADER_FILE) \
		$(LOADER_BINARY))):
	+$(call cmd,mkdir)


####################
#  INSTALL TARGET  #
####################

qcmd_copy_file = CP $(notdir $(2)) $(dir $@)
cmd_copy_file  = cp $(2) $@

PHONY += install
install: \
	$(PREFIX)/bin/$(notdir $(LOADER_FILE)) \
	$(PREFIX)/bin/$(notdir $(LOADER_BINARY))

$(PREFIX)/bin/$(notdir $(LOADER_FILE)): FORCE | $(PREFIX)/bin
	+$(call cmd,copy_file,$(LOADER_FILE))

$(PREFIX)/bin/$(notdir $(LOADER_BINARY)): FORCE | $(PREFIX)/bin
	+$(call cmd,copy_file,$(LOADER_BINARY))


###################
#  PHONY TARGETS  #
###################

PHONY += FORCE
FORCE:

.PHONY: $(PHONY)

