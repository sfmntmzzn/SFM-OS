
/* ----------------------------------------------------------------- *
 *
 *   stdbool.h: declarations and definitions for the boolean
 *              variable type.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LIBSTD_STDBOOL_HEADER
#define LIBSTD_STDBOOL_HEADER


#define bool	enum boolean_t


enum boolean_t
{
	false	= 0x00,
	true	= 0x01,
};


#endif // LIBSTD_STDBOOL_HEADER

