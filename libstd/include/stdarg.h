
/* ----------------------------------------------------------------- *
 *
 *   stdarg.h: declarations and definitions for variable
 *             argument lists.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LIBSTD_STDARG_HEADER
#define LIBSTD_STDARG_HEADER


#define va_start(v,l)		__builtin_va_start( v, l )
#define va_end(v)		__builtin_va_end( v )
#define va_arg(v,l)		__builtin_va_arg( v, l )
#define va_copy(d,s)		__builtin_va_copy( d, s )


typedef __builtin_va_list va_list;


#endif // LIBSTD_STDARG_HEADER

