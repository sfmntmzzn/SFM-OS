
/* ----------------------------------------------------------------- *
 *
 *   string.h: basic function declarations for several string
 *             and memory operations.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LIBSTD_STRING_HEADER
#define LIBSTD_STRING_HEADER

#include <stddef.h>


#ifndef NONNULL
#define NONNULL		__attribute__((nonnull))
#endif // NONNULL

#ifndef PURE
#define PURE		__attribute__((pure))
#endif // PURE

#ifndef MALLOC
#define MALLOC		__attribute__((malloc))
#endif // MALLOC

#ifndef CONST
#define CONST		__attribute__((const))
#endif // CONST


void* memcpy ( void*, const void*, size_t ) NONNULL;
void* memmove ( void*, const void*, size_t ) NONNULL;
void* memset ( void*, int, size_t ) NONNULL;
int memcmp ( void*, void*, size_t ) PURE NONNULL;
void* memchr ( void*, int, size_t ) PURE NONNULL;
void* memmem ( const void*, size_t, const void*, size_t ) PURE NONNULL;
void* mempcpy ( void*, const void*, size_t ) NONNULL;

char* strcpy ( char*, const char* ) NONNULL;
char* strncpy ( char*, const char*, size_t ) NONNULL;
char* strcat ( char*, const char* ) NONNULL;
char* strncat ( char*, const char*, size_t ) NONNULL;
int strcmp ( const char*, const char* ) PURE NONNULL;
int strncmp ( const char*, const char*, size_t ) PURE NONNULL;
int strcasecmp ( const char*, const char* ) PURE NONNULL;
int strncasecmp ( const char*, const char*, size_t ) PURE NONNULL;

char* strdup ( const char* ) MALLOC NONNULL;
char* strndup ( const char*, size_t ) MALLOC NONNULL;

char* strchr ( char*, int ) PURE NONNULL;
char* strrchr ( char*, int ) PURE NONNULL;
char* strchrnul ( char*, int ) PURE NONNULL;

size_t strspn ( const char*, const char* ) PURE NONNULL;
size_t strcspn ( const char*, const char* ) PURE NONNULL;
char* strpbrk ( const char*, const char* ) PURE NONNULL;
char* strstr ( const char*, const char* ) PURE NONNULL;
char* strtok ( char*, const char* ) PURE NONNULL;
char* strcasestr ( const char*, const char* ) PURE NONNULL;

size_t strlen ( const char* ) NONNULL;
size_t strnlen ( const char*, size_t ) NONNULL;

char* strerror ( int );
char* strerror_r ( int, char*, size_t ) NONNULL;

int ffs ( int ) CONST;
int ffsl ( long int ) CONST;
int ffsll ( long long int ) CONST;

int fls ( int ) CONST;
int flsl ( long int ) CONST;
int flsll ( long long int ) CONST;



#endif // LIBSTD_STRING_HEADER
