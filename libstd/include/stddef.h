
/* ----------------------------------------------------------------- *
 *
 *   stddef.h: provides common definitions and declarations.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LIBSTD_STDDEF_HEADER
#define LIBSTD_STDDEF_HEADER


#define NULL	(void*)( 0x00000000 )
#define FALSE	0x00
#define TRUE	0x01


// integer type definitions
typedef __PTRDIFF_TYPE__ ptrdiff_t;
typedef __SIZE_TYPE__ size_t;


#define offsetof(t,m)		__builtin_offsetof( t, m )


#endif // LIBSTD_STDDEF_HEADER

