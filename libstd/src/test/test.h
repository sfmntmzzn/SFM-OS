
/* ----------------------------------------------------------------- *
 *
 *   test.h: provides a minimalistic unit testing framework for
 *           the C language.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */

#ifndef LIBSTD_TEST_HEADER
#define LIBSTD_TEST_HEADER


#define TEST_STATUS_PASS	0x00
#define TEST_STATUS_FAIL	0x01

#define TEST_STRBUFFER_LENGTH	64


struct libstd_test_result
{
	int status;
	char error[ TEST_STRBUFFER_LENGTH ];
	unsigned long long int ticks;
};


#define CREATE_TEST(x)	\
struct libstd_test_result __libstd_test_ ## x ( void ) \
{ \
	struct libstd_test_result __local_test_result = \
	{ \
		.status = TEST_STATUS_PASS, \
		.error  = { 0 }, \
		.ticks = clock() \
	}; \


#define END_TEST	\
__local_test_end_label: \
	__local_test_result.ticks = clock() - __local_test_result.ticks; \
	return __local_test_result; \
} \


#define TEST_EXPECT_TRUE(x)	\
{ \
	if ( !(x) ) { \
		sprintf( __local_test_result.error, "%s != true", #x ); \
		__local_test_result.status = TEST_STATUS_FAIL; \
		goto __local_test_end_label; \
	} \
} \

#define TEST_EXPECT_FALSE(x)	\
{ \
	if ( (x) ) { \
		sprintf( __local_test_result.error, "%s != false", #x ); \
		__local_test_result.status = TEST_STATUS_FAIL; \
		goto __local_test_end_label; \
	} \
} \

#define TEST_EXPECT_EQUAL(x,y)	\
{ \
	if ( (x) != (y) ) { \
		sprintf( __local_test_result.error, "%s != %s", #x, #y ); \
		__local_test_result.status = TEST_STATUS_FAIL; \
		goto __local_test_end_label; \
	} \
} \

#define TEST_EXPECT_ARRAY_EQUAL(x,y,z)	\
{ \
	char* __p = (x), *__q = (y); \
	for ( int __n = 0; __n < (z); ++__n ) { \
		if ( __p[__n] != __q[__n] ) { \
			sprintf( __local_test_result.error, \
				"%s[%i] != %s[%i]", #x, __n, #y, __n ); \
			__local_test_result.status = TEST_STATUS_FAIL; \
			goto __local_test_end_label; \
		} \
	} \
} \


#define TEST_EXPECT_ARRAY_NOT_EQUAL(x,y,z)	\
{ \
	int __m = 0; \
	char* __p = (x), *__q = (y); \
	for ( int __n = 0; __n < (z); ++__n ) { \
		if ( __p[__n] == __q[__n] ) ++__m; \
		else break; \
	} \
	if ( __m == (z) ) { \
		sprintf( __local_test_result.error, "%s == %s", #x, #y ); \
		__local_test_result.status = TEST_STATUS_FAIL; \
		goto __local_test_end_label; \
	} \
} \


#define RUN_TEST(x)	\
printf( "[%s] RUN %s ", __libstd_test_suite_name, #x ); \
{ \
	struct libstd_test_result res = __libstd_test_##x (); \
	if ( res.status == TEST_STATUS_PASS ) { \
		printf( "\t\tPASS\t%llu.%03llus\n", res.ticks / 1000, \
			res.ticks % 1000 ); \
		__libstd_test_suite_passed++; \
	} else { \
		printf( "\t\tFAIL\n\t%s\n", res.error ); \
		__libstd_test_suite_failed++; \
	} \
} \


#define CREATE_TEST_SUITE(x)	\
{ \
	char* __libstd_test_suite_name = #x; \
	int __libstd_test_suite_failed = 0; \
	int __libstd_test_suite_passed = 0 \


#define END_TEST_SUITE(x)	\
	printf( "[%s] FINISHED \t\t%i FAILED / %i PASSED\n", \
		__libstd_test_suite_name, \
		__libstd_test_suite_failed, \
		__libstd_test_suite_passed ); \
} \


#define TEST_SUITE_HAS_PASSED		(__libstd_test_suite_failed==0)


extern char*	__libstd_test_suite_name;
extern int	__libstd_test_suite_failed;
extern int	__libstd_test_suite_passed;


extern int printf( const char*, ... );
extern int sprintf( char*, const char*, ... );
extern __UINT64_TYPE__ clock ( void );


#endif // LIBSTD_TEST_HEADER

