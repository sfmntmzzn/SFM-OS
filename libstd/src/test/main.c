
/* ----------------------------------------------------------------- *
 *
 *   test.c: unit tests for the C standard library of the
 *           SFM-OS kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include "test.h"

#include <stdlib.h>
#include <string.h>


// -------------------
// |   TEST: QSORT   |
// -------------------

int integer_sort ( const void* a, const void* b )
{
	return *(int*)a - *(int*)b;
}

CREATE_TEST(qsort_simple)
{
	// variables
	volatile int a[] = { 99, 39, 31, 18, 82, 6, 27, 73, 76, 11 };
	volatile int b[] = { 6, 11, 18, 27, 31, 39, 73, 76, 82, 99 };

	// sort `a' and test whether it matches `b'
	qsort( a, 10, sizeof(int), integer_sort );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST


// --------------------
// |   TEST: MEMCPY   |
// --------------------

CREATE_TEST(memcpy_simple)
{
	// variables
	volatile char a[] = 
		{ 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06, 0x6c, 0x24 };
	volatile char b[] = 
		{ 0xc8, 0xaf, 0x23, 0x91, 0x7c, 0x3b, 0x2e, 0x7e, 0xd9, 0xf3 };

	// copy `a' into `b' and test whether they are the same now
	memcpy( b, a, 10 );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST


// ---------------------
// |   TEST: MEMMOVE   |
// ---------------------

CREATE_TEST(memmove_simple)
{
	// variables
	volatile char a[] = 
		{ 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06, 0x6c, 0x24 };
	volatile char b[] = 
		{ 0xc8, 0xaf, 0x23, 0x91, 0x7c, 0x3b, 0x2e, 0x7e, 0xd9, 0xf3 };

	// move `a' into `b' and test whether they are the same now
	memmove( b, a, 10 );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST

CREATE_TEST(memmove_overlap)
{
	// variables
	volatile char a[] = 
		{ 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06, 0x6c, 0x24 };
	volatile char b[] = 
		{ 0x8a, 0x9b, 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06 };

	// move 8 bytes of `a' into `a+2'
	memmove( a + 2, a, 8 );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST


// --------------------
// |   TEST: MEMSET   |
// --------------------

CREATE_TEST(memset_simple)
{
	// variables
	volatile char a[] = 
		{ 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06, 0x6c, 0x24 };
	volatile char b[] = 
		{ 0x8a, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x06, 0x6c, 0x24 };

	// set 6 bytes at `a+1' to 0x1f and test whether `a'
	// and `b' are the same
	memset( a + 1, 0x1f, 6 );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST

CREATE_TEST(memset_aligned)
{
	// variables
	volatile char a[] = 
		{ 0x8a, 0x9b, 0xec, 0x91, 0x84, 0x5f, 0x79, 0x06, 0x6c, 0x24 };
	volatile char b[] = 
		{ 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x6c, 0x24 };

	// set 8 bytes at `a' to 0x54 and test whether `a'
	// and `b' are the same
	memset( a, 0x54, 8 );
	TEST_EXPECT_ARRAY_EQUAL( a, b, 10 );
}
END_TEST


// --------------------
// |   TEST: STRCMP   |
// --------------------

CREATE_TEST(strcmp_simple)
{
	// variables
	volatile char* a = "work";
	volatile char* b = "world";

	// test the lexicographical comparision of the
	// two strings
	TEST_EXPECT_TRUE( strcmp( a, b ) < 0 );
}
END_TEST

CREATE_TEST(strcmp_simple2)
{
	// variables
	volatile char* a = "Hello World! Test Test Test Test";
	volatile char* b = "Hello World! Test Test Test";

	// test the lexicographical comparision of the
	// two strings
	TEST_EXPECT_TRUE( strcmp( a, b ) > 0 );
}
END_TEST

CREATE_TEST(strcmp_equal)
{
	// variables
	volatile char* a = "Hello World!";
	volatile char* b = "Hello World!";

	// test the lexicographical comparision of the
	// two strings
	TEST_EXPECT_TRUE( strcmp( a, b ) == 0 );
}
END_TEST


// --------------------
// |   TEST: STRLEN   |
// --------------------

CREATE_TEST(strlen_empty)
{
	// variables
	volatile char* s = "";

	// test the string length of an empty string
	TEST_EXPECT_EQUAL( strlen( s ), 0 );
}
END_TEST

CREATE_TEST(strlen_simple)
{
	// variables
	volatile char* s = "Hello World!";

	// test the string length of 'Hello World!'
	TEST_EXPECT_EQUAL( strlen( s ), 12 );
}
END_TEST


// --------------------
// |   TEST: STRNCMP   |
// --------------------

CREATE_TEST(strncmp_simple)
{
	// variables
	volatile char* a = "work";
	volatile char* b = "world";

	// test the lexicographical comparision of the
	// two strings
	TEST_EXPECT_TRUE( strncmp( a, b, 128 ) < 0 );
}
END_TEST

CREATE_TEST(strncmp_equal)
{
	// variables
	volatile char* a = "work";
	volatile char* b = "world";

	// test the lexicographical comparision of the
	// two strings
	TEST_EXPECT_TRUE( strncmp( a, b, 0 ) == 0 );
}
END_TEST


// -------------------------------------
// |   MAIN - TEST SUITE ENTRY POINT   |
// -------------------------------------

int main ( void )
{
	// the local variable for the function
	int has_passed;

	// put the test suite in its own block
	{
		// initialize the test suite
		CREATE_TEST_SUITE(libstdtest);

		// run all tests
		RUN_TEST(qsort_simple);
		RUN_TEST(memcpy_simple);
		RUN_TEST(memmove_simple);
		RUN_TEST(memmove_overlap);
		RUN_TEST(memset_simple);
		RUN_TEST(memset_aligned);
		RUN_TEST(strcmp_simple);
		RUN_TEST(strcmp_simple2);
		RUN_TEST(strcmp_equal);
		RUN_TEST(strlen_empty);
		RUN_TEST(strlen_simple);
		RUN_TEST(strncmp_simple);
		RUN_TEST(strncmp_equal);

		// save the `TEST_SUITE_HAS_PASSED' value
		has_passed = TEST_SUITE_HAS_PASSED;

		// call the end of the testsuite
		END_TEST_SUITE(libstdtest);
	}

	// return 0 if `has_passed' is 1
	return 1 - has_passed;
}

