
/* ----------------------------------------------------------------- *
 *
 *   memset.c: set a memory area to a specific byte value.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stddef.h>
#include <stdint.h>
#include <string.h>


#define WORDSIZE		sizeof(size_t)
#define UNALIGNED(x)		( (uintptr_t)( (x) ) & ( WORDSIZE - 1 ) )


void* memset ( void* d, int v, size_t n )
{
	// variables for the algorithm
	uint8_t x = v & 0xFF;
	uint8_t* p = (uint8_t*)( d );

#ifdef __OPTIMIZE__

	// more variables
	size_t w = ( x << 24 ) | ( x << 16 ) | ( x << 8 ) | x;

	// loop the first few bytes until we are word
	// aligned
	while ( __builtin_expect( UNALIGNED( p ), FALSE ) )
	{
		// do we still have bytes to set?
		if ( __builtin_expect( n--, TRUE ) )
		{
			// set the current byte to the
			// specified value
			*p++ = x;
		}
		else
		{
			// return `d'
			return d;
		}
	}

	// set `q' to point to the word aligned
	// string pointer
	size_t* q = (size_t*)( p );

	// loop until the amount of bytes left to set isn't
	// big enough anymore
	while ( __builtin_expect( n >= WORDSIZE, TRUE ) )
	{
		// set the value for the whole word at once
		*q++ = w;

		// decrease `n' by the word size
		n -= WORDSIZE;
	}

	// update `p'
	p = (uint8_t*)( q );

#endif // __OPTIMIZE__
	
	// handle the remaining bytes
	while ( __builtin_expect( n--, TRUE ) )
	{
		// set the current byte to the
		// specified value
		*p++ = x;
	}

	// return `d'
	return d;
}

