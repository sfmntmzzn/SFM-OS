
/* ----------------------------------------------------------------- *
 *
 *   memmove.c: move a memory area to another location.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stddef.h>
#include <stdint.h>
#include <string.h>


#define WORDSIZE		sizeof(size_t)
#define UNALIGNED(x)		( (uintptr_t)( (x) ) & ( WORDSIZE - 1 ) )
#define UNALIGNED2(x,y)		( UNALIGNED( (x) ) || UNALIGNED( (y) ) )


void* memmove ( void* d, const void* s, size_t n )
{
	// variables for the algorithm
	uint8_t* p = (uint8_t*)( d ), *q = (uint8_t*)( s );

	// test whether we might run into a destructive
	// overlapping of the memory areas
	if ( __builtin_expect( p > q && p < q + n, FALSE ) )
	{
		// shift both pointers by `n'
		p += n;
		q += n;

		// loop through the areas byte by byte
		while ( __builtin_expect( n--, TRUE ) )
		{
			// copy the current byte to the
			// destination
			*--p = *--q;
		}
	}
	else
	{
#ifdef __OPTIMIZE__
		// loop the first few bytes until we are
		// word aligned
		while ( __builtin_expect( UNALIGNED2( p, q ), TRUE ) )
		{
			// check whether we already copied
			// enough bytes
			if ( __builtin_expect( !n, FALSE ) )
			{
				// return the original destination
				// pointer
				return d;
			}

			// copy the byte over to the destination
			*p++ = *q++;
		}

		// declare the wordsize variants of `p' and `q'
		size_t* _p = (size_t*)( p );
		size_t* _q = (size_t*)( q );

		// loop until the amount left to copy isn't big
		// enough anymore
		while ( __builtin_expect( n >= WORDSIZE, TRUE ) )
		{
			// copy the word over to the destination
			*_p++ = *_q++;

			// decrease `n' by the word size
			n -= WORDSIZE;
		}

		// update the variables `_p' and `_q'
		p = (uint8_t*)( _p );
		q = (uint8_t*)( _q );

#endif // __OPTIMIZE__

		// loop until we have copied enough bytes
		while ( __builtin_expect( n--, TRUE ) )
		{
			// copy the byte over to the destination
			*p++ = *q++;
		}
	}

	// return the original destination
	// pointer
	return d;
}

