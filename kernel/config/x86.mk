
#
#
#   x86.mk: configuration for the x86 architecture.
#
#   Copyright (C) 2018 Sandro Francesco Montemezzani
#   Author: Sandro Montemezzani <sandro@montemezzani.net>
#
#   This file is part of the SFM-OS project, and is licensed
#   under the MIT License.
#
#


########################
#  UPDATE FILES SETUP  #
########################

LINKER_SCRIPT_RECEIPT = $(configdir)/i386.lds.h


#############################
#  UPDATE COMPILER OPTIONS  #
#############################

CROSS_CFLAGS   += -m32 -fno-unwind-tables -fno-asynchronous-unwind-tables \
                  -fno-stack-protector -freg-struct-return
CROSS_CPPFLAGS += -D__i386__


#######################
#  COMPILATION UNITS  #
#######################

OBJECT_FILES += \
	$(builddir)/obj/arch/x86/gdt.o \
	$(builddir)/obj/arch/x86/gdt_flush.o \
	$(builddir)/obj/arch/x86/gdt_init.o \
	$(builddir)/obj/arch/x86/gdt_set.o \
	$(builddir)/obj/arch/x86/idt.o \
	$(builddir)/obj/arch/x86/idt_flush.o \
	$(builddir)/obj/arch/x86/idt_init.o \
	$(builddir)/obj/arch/x86/idt_set.o \
	$(builddir)/obj/arch/x86/irq.o \
	$(builddir)/obj/arch/x86/irq_handler.o \
	$(builddir)/obj/arch/x86/irq_init.o \
	$(builddir)/obj/arch/x86/irq_set.o \
	$(builddir)/obj/arch/x86/irq_stub.o \
	$(builddir)/obj/arch/x86/irq_unset.o \
	$(builddir)/obj/arch/x86/isr.o \
	$(builddir)/obj/arch/x86/isr_handler.o \
	$(builddir)/obj/arch/x86/isr_init.o \
	$(builddir)/obj/arch/x86/isr_set.o \
	$(builddir)/obj/arch/x86/isr_stub.o \
	$(builddir)/obj/arch/x86/isr_unset.o \
	$(builddir)/obj/arch/x86/kernel_start.o \
	$(builddir)/obj/arch/x86/multiboot.o \
	$(builddir)/obj/io/arch/x86/serial_init.o \
	$(builddir)/obj/io/arch/x86/serial_putchar.o \
	$(builddir)/obj/io/arch/x86/text.o \
	$(builddir)/obj/io/arch/x86/text_init.o \
	$(builddir)/obj/io/arch/x86/text_putchar.o

