#!/bin/bash


INPUT_FILE="$1"
OUTPUT_FILE="$2"


export GIT_HASH="$(git rev-parse --quiet --verify --short HEAD)"
export GIT_BRANCH="$(git symbolic-ref --quiet --short HEAD || echo $GIT_HASH)"
export GIT_TAG=$(git describe --tags --abbrev=0)
export GIT_OFFSET=$(git rev-list --count ${GIT_TAG}..HEAD)

export USER_NAME="$(whoami)"
export DOMAIN_NAME="$(domainname)"

export BUILD_YEAR=$(date '+%Y')
export BUILD_MONTH=$(date '+%-m')
export BUILD_DAY=$(date '+%-d')

export HOST_SYSTEM="$(uname -s)"


VERSION_STRING="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}"
[[ ${GIT_OFFSET} = 0 ]] || VERSION_STRING+="-${GIT_OFFSET}"
[[ "${GIT_BRANCH}" = "master" ]] || VERSION_STRING+="-${GIT_BRANCH}"
VERSION_STRING+=" (${GIT_HASH}) $(date '+%F')"
export VERSION_STRING


BUILD_INVOKER="${USER_NAME}"
[[ -n "${DOMAIN_NAME}" ]] && BUILD_INVOKER+=" at ${DOMAIN_NAME}"
export BUILD_INVOKER


awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3); \
	gsub("[$]{"var"}",ENVIRON[var])}}1' < ${INPUT_FILE} > ${OUTPUT_FILE}

