
/* ----------------------------------------------------------------- *
 *
 *   version.h: symbol definitions with version and build
 *              information for the SFM-OS kernel
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_VERSION_HEADER
#define KERNEL_VERSION_HEADER

#define LOADER_VERSION_MCRQUOTE(name)	#name
#define LOADER_VERSION_MCRSTR(macro)	LOADER_VERSION_MCRQUOTE(macro)

#define KERNEL_VERSION_STRING	"${VERSION_STRING}"

#define KERNEL_VERSION_MAJOR	${VERSION_MAJOR}
#define KERNEL_VERSION_MINOR	${VERSION_MINOR}
#define KERNEL_VERSION_PATCH	${VERSION_PATCH}

#define KERNEL_BUILD_BRANCH	"${GIT_BRANCH}"
#define KERNEL_BUILD_HASH	"${GIT_HASH}"
#define KERNEL_BUILD_TAG	"${GIT_TAG}"
#define KERNEL_BUILD_OFFSET	${GIT_OFFSET}

#define KERNEL_BUILD_USER	"${USER_NAME}"
#define KERNEL_BUILD_DOMAIN	"${DOMAIN_NAME}"
#define KERNEL_BUILD_INVOKER	"${BUILD_INVOKER}"

#define KERNEL_BUILD_YEAR	${BUILD_YEAR}
#define KERNEL_BUILD_MONTH	${BUILD_MONTH}
#define KERNEL_BUILD_DAY	${BUILD_DAY}

#define KERNEL_BUILD_ARCH	"${BUILD_ARCH}"
#define KERNEL_BUILD_HOST	"${HOST_SYSTEM}"

#ifdef __GNUC__
#define KERNEL_BUILD_COMPILER	\
	"GCC " LOADER_VERSION_MCRSTR(__GNUC__) \
	"." LOADER_VERSION_MCRSTR(__GNUC_MINOR__) \
	"." LOADER_VERSION_MCRSTR(__GNUC_PATCHLEVEL__)
#else
#define KERNEL_BUILD_COMPILER	"unknown"
#endif // __GNUC__

#endif // KERNEL_VERSION_HEADER

