
/* ----------------------------------------------------------------- *
 *
 *   kernel_init.c: initializes the kernel.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>
#include <arch/x86/idt.h>
#include <arch/x86/irq.h>
#include <arch/x86/isr.h>
#include <boot/multiboot.h>
#include <io/output.h>
#include <io/serial.h>
#include <io/text.h>
#include <sys/meminfo.h>
#include <sys/mmap.h>
#include <sys/pageframe.h>
#include <sys/pit.h>
#include <version.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>


#ifndef CDECL
#define CDECL		__attribute__((cdecl,regparm(0)))
#endif // CDECL


#define MULTIBOOT_TAG_ALIGNMENT	8
#define TAG_ALIGN(x)	( (x) = (__typeof__(x))( ( (uintptr_t)(x) \
	+ MULTIBOOT_TAG_ALIGNMENT - 1 ) & ( -MULTIBOOT_TAG_ALIGNMENT ) ) )


#define KERNEL_ERROR_INCORRECT_MAGIC	0x01
#define KERNEL_ERROR_INVALID_MBI	0x02

#define PIT_TIMER_FREQUENCY		20


extern int __kernel_start;
extern int __kernel_end;


CDECL int kernel_init ( const multiboot_uint32_t magic,
	struct multiboot_info* const mbi )
{
	// compare the number in the first argument to the
	// magic number specified by the multiboot2 specification
	if ( __builtin_expect( magic != MULTIBOOT_INFO_MAGIC, FALSE ) )
	{
		// we have a incorrect magic number. We have
		// to return a error
		return KERNEL_ERROR_INCORRECT_MAGIC;
	}

	// test the multiboot info pointer for being a nullpointer
	if ( __builtin_expect( mbi == 0x00000000, FALSE ) )
	{
		// the passed multiboot info pointer was a nullpointer.
		// Return the corresponding error code.
		return KERNEL_ERROR_INVALID_MBI;
	}

	// create the MMAP entry for the multiboot info passed
	// by the bootloader
	struct mmap_entry mbi_mmap_entry = 
	{
		.base = (uintptr_t)( mbi ),
		.length = (uintptr_t)( mbi ) + mbi->total_size,
		.type = MMAP_TYPE_RESERVED,
		.zero = 0x00000000
	};
	mmap_insert( &mbi_mmap_entry );

	// create the MMAP entry for the kernel space
	// (.text .data .bss etc.)
	struct mmap_entry kernel_mmap_entry = 
	{
		.base = (uintptr_t)( &__kernel_start ),
		.length = (uintptr_t)( &__kernel_end )
			- (uintptr_t)( &__kernel_start ),
		.type = MMAP_TYPE_RESERVED,
		.zero = 0x00000000
	};
	mmap_insert( &kernel_mmap_entry );

	// create some local string pointer that point to
	// information passed by multiboot
	char* command_line = { '\0' };
	char* bootloader_name = { '\0' };

	// parse the provided multiboot info tags
	for ( char* p = (char*)( mbi->tags );
		p < (char*)( mbi ) + mbi->total_size; )
	{
		// create a dedicated pointer for the current info tag
		struct multiboot_info_tag* q =
			(struct multiboot_info_tag*)( p );

		// switch through all possible multiboot info tags
		switch ( q->type )
		{
		case MULTIBOOT_TAG_END:
			// we have reached the end of the multiboot
			// info tags, so jump out of the loop
			goto next;
		case MULTIBOOT_TAG_CMDLINE:
			// store the command line into the variable
			// we created above
			command_line = (char*)( (
				(struct multiboot_info_tag_command_line*)(
				q ) )->string );
			break;
		case MULTIBOOT_TAG_BOOTLOADER:
			// store the bootloader name into the variable
			// we created above
			bootloader_name = (char*)( (
				(struct multiboot_info_tag_bootloader_name*)(
				q ) )->string );
			break;
		case MULTIBOOT_TAG_MEMORY_INFO:
			// set the corresponding fields in the
			// kernel `mem_info' structure and also
			// add those regions to the kernel MMAP
			// structure
			kernel_meminfo.lower =
				( (struct multiboot_info_tag_memory*)( q )
				)->mem_lower;
			kernel_meminfo.upper =
				( (struct multiboot_info_tag_memory*)( q )
				)->mem_upper;
			// and also add those regions to the
			// kernel MMAP structure
			{
				// insert the lower memory region.
				// Shift the value to the left by
				// 10 as it is represented in KiB
				struct mmap_entry e = 
				{
					.base = 0x00000000,
					.length = kernel_meminfo.lower << 10,
					.type = MMAP_TYPE_FREE,
					.zero = 0x00
				};
				mmap_insert( &e );
			}
			{
				// insert the upper memory region.
				// Shift the value to the left by
				// 10 as it is represented in KiB
				struct mmap_entry e = 
				{
					.base = 0x00100000,
					.length = kernel_meminfo.upper << 10,
					.type = MMAP_TYPE_FREE,
					.zero = 0x00
				};
				mmap_insert( &e );
			}
			break;
		case MULTIBOOT_TAG_MMAP:
			// read each entry seperatly into the kernel
			// mmap. That way we can construct a sanitized
			// MMAP structure, and don't have to bother later
			// TODO: just copy the structure as a whole, we
			// still need to sanitize it anyway
			for ( struct mmap_entry* e = (struct mmap_entry*)(
				( (struct multiboot_info_tag_mmap*)(
				q ) )->entries ); e < (struct mmap_entry*)(
				p + q->size ); ++e )
			{
				// insert the mmap entry
				mmap_insert( e );
			}
			break;
		}

		// increase and align `p'
		p += q->size;
		TAG_ALIGN( p );
	}

next:
	// parse the command line
	{
		// variable
		char* p = command_line;

	parse_next_arg:
		// loop until we reach the first non-whitespace
		// character
		while ( __builtin_expect( *p == ' ', TRUE ) )
		{
			// increase `p'
			++p;
		}

		// switch through the possible options
		if ( __builtin_expect( !strncmp( p, "debug", 5 ), FALSE ) )
		{
			// increase `p'
			p += 5;

			// variables
			bool parse_next = false;

			// check whether a '=' follows
			if ( __builtin_expect( *p == '=', TRUE ) )
			{
				// increase `p'
				++p;

				// update `parse_next'
				parse_next = true;
			}

			// loop until `parse_next' is false
			while ( __builtin_expect( parse_next, TRUE ) )
			{
				// switch through the possible
				// option values
				if ( __builtin_expect(
					!strncmp( p, "serial", 6 ), FALSE ) )
				{
					// increase `p'
					p += 6;

					// update `kernel_active_debug_mode'
					kernel_active_debug_mode |=
						DEBUG_CHANNEL_SERIAL;
				}
				else if ( __builtin_expect(
					!strncmp( p, "text", 4 ), FALSE ) )
				{
					// increase `p'
					p += 4;

					// update `kernel_active_debug_mode'
					kernel_active_debug_mode |=
						DEBUG_CHANNEL_TEXT;
				}
				else
				{
					// break
					break;
				}

				// check whether a ',' follows
				if ( __builtin_expect( *p == ',', TRUE ) )
				{
					// increase `p'
					++p;
				}
				else
				{
					// update `parse_next'
					parse_next = false;
				}
			}
		}

		// loop until we reach the next whitespace
		// character
		while ( __builtin_expect( *p && *p != ' ', FALSE ) )
		{
			// increase `p'
			++p;
		}

		// check whether we reached the NUL byte
		if ( __builtin_expect( *p != '\0', TRUE ) )
		{
			goto parse_next_arg;
		}
	}

	// check whether the serial debug channel is active
	if ( __builtin_expect( kernel_active_debug_mode
		& DEBUG_CHANNEL_SERIAL, TRUE ) )
	{
		// initialize the serial port for debug printing
		serial_init( SERIAL_COM1_PORT, DEFAULT_BAUD_RATE );
	}

	// check whether the text debug channel is active
	if ( __builtin_expect( kernel_active_debug_mode
		& DEBUG_CHANNEL_TEXT, TRUE ) )
	{
		// clear the text buffer
		text_init();
	}

	// print the version information for this
	// SFM-OS build to the screen
	kernel_putstring( "SFM-OS " KERNEL_VERSION_STRING "\nBuild by "
		KERNEL_BUILD_INVOKER " using " KERNEL_BUILD_COMPILER
		" on " KERNEL_BUILD_HOST "\n\n" );

	// print the bootloader name and command line
	kernel_printf( "Bootloader: %s\n", bootloader_name );
	kernel_printf( "Cmdline: %s\n", command_line );

	// insert the MMAP entry for the multiboot info area
	kernel_putstring( "Kernel: sanitize the MMAP structure..." );
	mmap_sanitize();
	kernel_putstring( " Done.\n" );

	// print the total amount of available RAM on
	// this system
	{
		uint64_t mem_total = 0, mem_avail = 0;
		for ( struct mmap_entry* p = kernel_mmap.start;
			p != kernel_mmap.end; ++p )
		{
			mem_total += p->type != MMAP_TYPE_UNDEFINED
				? p->length : 0;
			mem_avail += p->type == MMAP_TYPE_FREE
				? p->length : 0;
		}
		kernel_printf( "Kernel: detected RAM: total %llu KiB"
			", available %llu KiB\n",
			mem_total >> 10, mem_avail >> 10 );
	}

	// print the current MMAP structure to the screen
	kernel_putstring( "Kernel: print out the kernel MMAP structure:\n" );
	mmap_print( "MMAP:" );

	// setup the global descriptor table for the kernel
	kernel_putstring( "Kernel: init global descriptor table..." );
	gdt_init();
	kernel_putstring( " Done.\n" );

	// setup the interrupt descriptor table for the kernel
	kernel_putstring( "Kernel: init interrupt table..." );
	idt_init();
	isr_init();
	irq_init();
	idt_flush();
	kernel_putstring( " Done.\n" );

	// setup the pageframe allocator for the kernel
	kernel_putstring( "Kernel: init pageframe allocator..." );
	pageframe_init();
	kernel_putstring( " Done.\n" );

	// print some pageframe buddy system information
	kernel_printf( "Kernel: allocation buddies: %i",
		kernel_pageframe_buddies[ 0 ].order );
	for ( int i = 1; i != kernel_pageframe_buddy_count; ++i )
	{
		kernel_printf( ",%i", kernel_pageframe_buddies[ i ].order );
	}
	kernel_putstring( "\n" );

	// setup the programmable interval timer for the kernel
	kernel_putstring( "Kernel: init global interrupt timer..." );
	pit_init();
	pit_set_frequency( PIT_TIMER_FREQUENCY );
	kernel_putstring( " Done.\n" );
	kernel_printf( "Kernel: interrupt timer frequency: %iHz\n",
		PIT_TIMER_FREQUENCY );

	// as we reached the end of the function we can
	// assume that all operations were successfull
	return 0;
}

