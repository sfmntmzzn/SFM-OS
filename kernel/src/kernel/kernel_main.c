
/* ----------------------------------------------------------------- *
 *
 *   kernel_main.c: the main function of the kernel. This one
 *                  does not return when it is called.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <sys/pageframe.h>


#ifndef CDECL
#define CDECL		__attribute__((cdecl))
#endif // CDECL

#ifndef NORETURN
#define NORETURN	__attribute__((noreturn))
#endif // NORETURN


CDECL NORETURN void kernel_main ( void )
{
	int i = pageframe_allocate( 10 );
	kernel_printf( "%i\n", i );
	kernel_printf( "%i\n", pageframe_allocate( 100 ) );
	pageframe_deallocate( i, 10 );
	kernel_printf( "%i\n", pageframe_allocate( 10 ) );

	// infinitely loop here as we don't want to
	// to return from this function
	for ( ;; );

	// signalize the compiler we won't get to
	// this point
	__builtin_unreachable();
}
