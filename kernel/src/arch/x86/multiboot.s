
/* ----------------------------------------------------------------- *
 *
 *   multiboot.s: implements the multiboot header structure
 *                for the kernel binary. More information at:
 *
 *   https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern __bss_end
.extern __data_end
.extern __kernel
.extern kernel_start

// include the multiboot header file, which contains
// all the relevant multiboot definitions
.include "boot/multiboot.inc"

.set	MB_MAGIC_NUMBER, MULTIBOOT_HEADER_MAGIC
.set	MB_ARCHITECTURE, MULTIBOOT_ARCH_I386
.set	MB_HEADERLENGTH, ( __multiboot_header.end - __multiboot_header )
.set	MB_CHECKSUM, -( MB_MAGIC_NUMBER + MB_ARCHITECTURE + MB_HEADERLENGTH )

.set	MB_CONSOLE_FLAGS, MULTIBOOT_CONSOLE_FLAG_REQUIRED
.set	MB_CONSOLE_FLAGS, ( MB_CONSOLE_FLAGS | MULTIBOOT_CONSOLE_FLAG_EGA )


.code32
.org 0x00

.section .multiboot, "aw", @progbits

.global __multiboot_header
.type __multiboot_header, @object

__multiboot_header:
	// ------------------------
	// |   MULTIBOOT HEADER   |
	// ------------------------

	// magic fields of the multiboot header
	.long	MB_MAGIC_NUMBER		/* 00..03: magic number (0xe85250d6) */
	.long	MB_ARCHITECTURE		/* 04..07: architecture (i386) */
	.long	MB_HEADERLENGTH		/* 08..0B: header length */
	.long	MB_CHECKSUM		/* 0C..0F: checksum */


__info_request_tag:
	// ------------------------
	// |   INFO REQUEST TAG   |
	// ------------------------

	.short	MULTIBOOT_HEADER_TAG_INFO_REQUEST
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __info_request_tag.end - __info_request_tag )
	.long	MULTIBOOT_TAG_CMDLINE
	.long	MULTIBOOT_TAG_MEMORY_INFO
	.long	MULTIBOOT_TAG_BOOTLOADER
	.long	MULTIBOOT_TAG_MMAP
	.long	MULTIBOOT_TAG_VBE
	.long	MULTIBOOT_TAG_FRAMEBUFFER

__info_request_tag.end:


__address_tag:
	// -------------------
	// |   ADDRESS TAG   |
	// -------------------

	.short	MULTIBOOT_HEADER_TAG_ADDRESS
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __address_tag.end - __address_tag )
	.long	__multiboot_header
	.long	__kernel
	.long	__data_end
	.long	__bss_end

__address_tag.end:


__entry_tag:
	// -----------------
	// |   ENTRY TAG   |
	// -----------------

	.short	MULTIBOOT_HEADER_TAG_ENTRY
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __entry_tag.end - __entry_tag )
	.long	kernel_start

__entry_tag.end:


__console_flags_tag:
	// -------------------------
	// |   CONSOLE FLAGS TAG   |
	// -------------------------

	.short	MULTIBOOT_HEADER_TAG_CONSOLE_FLAGS
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __console_flags_tag.end - __console_flags_tag )
	.long	MB_CONSOLE_FLAGS

__console_flags_tag.end:


__framebuffer_tag:
	// -----------------------
	// |   FRAMEBUFFER TAG   |
	// -----------------------

	.short	MULTIBOOT_HEADER_TAG_FRAMEBUFFER
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __framebuffer_tag.end - __framebuffer_tag )
	.long	80
	.long	25
	.long	0

__framebuffer_tag.end:


__module_align_tag:
	// ------------------------
	// |   MODULE ALIGN TAG   |
	// ------------------------

	.short	MULTIBOOT_HEADER_TAG_MODULE_ALIGN
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __module_align_tag.end - __module_align_tag )

__module_align_tag.end:


__end_tag:
	// ---------------
	// |   END TAG   |
	// ---------------

	.short	MULTIBOOT_HEADER_TAG_END
	.short	MULTIBOOT_HEADER_FLAG_NONE
	.long	( __end_tag.end - __end_tag )

__end_tag.end:


__multiboot_header.end:

