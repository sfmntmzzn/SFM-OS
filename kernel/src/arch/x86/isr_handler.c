
/* ----------------------------------------------------------------- *
 *
 *   isr_handler.c: implements the top handler that gets called
 *                  for every interrupt service routine and that
 *                  then itself calls the installed handler.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/irq.h>
#include <arch/x86/isr.h>
#include <io/output.h>
#include <sys/io.h>

#include <stddef.h>


#ifndef CDECL
#define CDECL		__attribute__((cdecl))
#endif // CDECL


#ifndef NORETURN
#define NORETURN	__attribute__((noreturn))
#endif // NORETURN


const char* isr_names[ ISR_NUM_HANDLERS ] = 
{
	"Division By Zero",
	"Debug",
	"Non-Maskable Interrupt",
	"Breakpoint",
	"Detected Overflow",
	"Out-of-Bounds",
	"Invalid Opcode",
	"No Coprocessor",
	"Double Fault",
	"Coprocessor Segment Overrun",
	"Bad TSS",
	"Segment Not Present",
	"Stack Fault",
	"General Protection Fault",
	"Page Fault",
	"Unknown Interrupt",
	"Coprocessor Fault",
	"Alignment Check",
	"Machine Check",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved"
};


CDECL NORETURN void isr_handler ( struct cpu_state* state )
{
	// test whether the interrupt index a valid
	// interrupt service routine index
	if ( __builtin_expect( state->int_no >= ISR_NUM_HANDLERS, FALSE ) )
	{
		// if not, move on with the infinite loop
		goto infinite_loop;
	}

	// print the exception message
	kernel_printf( "\n+++ Kernel Exception: %s [%lu] +++\n",
		isr_names[ state->int_no ], state->int_no );

	// dump the current context of the cpu when
	// the exception was fired
	kernel_printf( "GS=%04lx FS=%04lx ES=%04lx DS=%04lx\n"
		"EDI=%08lx ESI=%08lx EBP=%08lx ESP=%08lx\n"
		"EBX=%08lx EDX=%08lx ECX=%08lx EAX=%08lx\n"
		"INT_NO=%lu ERR_CODE=%li\n"
		"EIP=%08lx CS=%04lx EFLAGS=%lu USERESP=%08lx SS=%04lx\n",
		state->gs, state->fs, state->es, state->ds, state->edi,
		state->esi, state->ebp, state->esp, state->ebx, state->edx,
		state->ecx, state->eax, state->int_no, state->err_code,
		state->eip, state->cs, state->eflags, state->useresp,
		state->ss );

	// get the handler for the specified interrupt index
	void (*handler)( struct cpu_state* ) = kernel_isrs[ state->int_no ];

	// check if a handler for this interrupt has
	// been installed
	if ( __builtin_expect( handler != NULL, FALSE ) )
	{
		// execute the installed handler
		handler( state );
	}

infinite_loop:
	// infinitely loop
	for ( ;; );

	// signalize the compiler we won't reach this point
	__builtin_unreachable();
}

