
/* ----------------------------------------------------------------- *
 *
 *   irq_set.c: installs a interrupt request entry into
 *              the interrupt descriptor table structure
 *              of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/irq.h>

#include <stddef.h>
#include <stdint.h>


void irq_set ( uint32_t index, void (*handler)( struct cpu_state* ) )
{
	// test whether a valid index has been passed
	if ( __builtin_expect( index >= IRQ_NUM_HANDLERS, FALSE ) )
	{
		// if not, we should immediately return
		return;
	}

	// get the contents of the EFLAGS register
	uint32_t eflags = 0x00000000;
	__asm__ __volatile__ ( "pushf; pop %0" : "=rm"(eflags) :: "memory" );

	// check whether interrupt were enabled
	if ( __builtin_expect( eflags & ( 1UL << 9 ), TRUE ) )
	{
		// disable interrupts
		__asm__ __volatile__ ( "cli" ::: "memory" );
	}

	// set the right field the requests array
	kernel_irqs[ index ] = handler;

	// check whether interrupt were enabled
	if ( __builtin_expect( eflags & ( 1UL << 9 ), TRUE ) )
	{
		// reenable interrupts
		__asm__ __volatile__ ( "sti" ::: "memory" );
	}
}

