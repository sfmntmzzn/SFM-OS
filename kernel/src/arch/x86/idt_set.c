
/* ----------------------------------------------------------------- *
 *
 *   idt_set.c: installs a interrupt descriptor entry
 *              into the interrupt descriptor table structure
 *              of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/idt.h>

#include <stddef.h>
#include <stdint.h>


void idt_set ( uint32_t index, void (*handler)( void ),
	uint16_t selector, uint8_t attribute )
{
	// test whether a valid index has been passed
	if ( __builtin_expect( index >= IDT_NUM_ENTRIES, FALSE ) )
	{
		// if not, we should immediately return
		return;
	}

	// retrieve the passed handler address as integer
	uint32_t handler_addr = (uint32_t)( handler );

	// write all values into their corresponding field
	// in the interrupt descriptor entry structure
	kernel_idt[ index ].base_lo = handler_addr & 0xFFFF;
	kernel_idt[ index ].selector = selector;
	kernel_idt[ index ].zero = 0x00;
	kernel_idt[ index ].attribute = attribute;
	kernel_idt[ index ].base_hi = ( handler_addr >> 16 ) & 0xFFFF;
}

