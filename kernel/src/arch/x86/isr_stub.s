
/* ----------------------------------------------------------------- *
 *
 *   isr_stub.s: defines the interrupt service routine stubs
 *               for all 32 interrupt service routines.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern isr_handler


.code32
.org 0x00

.section .text

.type isr_stub_common, @function

isr_stub_common:
	// -----------------------
	// |   ISR_STUB_COMMON   |
	// -----------------------

	// push all registers
	pusha

	// save the segment registers to the stack
	push	%ds
	push	%es
	push	%fs
	push	%gs

	// set the segment register to 0x10, the kernel
	// data segment
	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %fs
	mov	%eax, %gs

	// make sure the direction flag is cleared
	cld

	// call the interrupt service routine handler
	mov	%esp, %eax
	push	%eax
	call	isr_handler
	add	$4, %esp

	// restore the segment registers from the stack
	pop	%gs
	pop	%fs
	pop	%es
	pop	%ds

	// restore the rest of the registers
	popa

	// pop the previously pushed error code and
	// interrupt index
	add	$8, %esp

	// return
	iret



.macro ISR_STUB_ERROR num
.global isr_stub\num
.type isr_stub\num, @function
isr_stub\num:
	cli
	push	$\num
	jmp	isr_stub_common
.endm

.macro ISR_STUB_NOERR num
.global isr_stub\num
.type isr_stub\num, @function
isr_stub\num:
	cli
	push	$0x00
	push	$\num
	jmp	isr_stub_common
.endm


ISR_STUB_NOERR 0
ISR_STUB_NOERR 1
ISR_STUB_NOERR 2
ISR_STUB_NOERR 3
ISR_STUB_NOERR 4
ISR_STUB_NOERR 5
ISR_STUB_NOERR 6
ISR_STUB_NOERR 7
ISR_STUB_ERROR 8
ISR_STUB_NOERR 9
ISR_STUB_ERROR 10
ISR_STUB_ERROR 11
ISR_STUB_ERROR 12
ISR_STUB_ERROR 13
ISR_STUB_ERROR 14
ISR_STUB_NOERR 15
ISR_STUB_NOERR 16
ISR_STUB_NOERR 17
ISR_STUB_NOERR 18
ISR_STUB_NOERR 19
ISR_STUB_NOERR 20
ISR_STUB_NOERR 21
ISR_STUB_NOERR 22
ISR_STUB_NOERR 23
ISR_STUB_NOERR 24
ISR_STUB_NOERR 25
ISR_STUB_NOERR 26
ISR_STUB_NOERR 27
ISR_STUB_NOERR 28
ISR_STUB_NOERR 29
ISR_STUB_NOERR 30
ISR_STUB_NOERR 31

