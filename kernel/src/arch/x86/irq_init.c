
/* ----------------------------------------------------------------- *
 *
 *   irq_init.c: initializes the interrupt requests for
 *               the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>
#include <arch/x86/idt.h>
#include <arch/x86/irq.h>
#include <arch/x86/isr.h>
#include <sys/io.h>

#include <stddef.h>
#include <string.h>


extern void irq_stub0 ( void );
extern void irq_stub1 ( void );
extern void irq_stub2 ( void );
extern void irq_stub3 ( void );
extern void irq_stub4 ( void );
extern void irq_stub5 ( void );
extern void irq_stub6 ( void );
extern void irq_stub7 ( void );
extern void irq_stub8 ( void );
extern void irq_stub9 ( void );
extern void irq_stub10 ( void );
extern void irq_stub11 ( void );
extern void irq_stub12 ( void );
extern void irq_stub13 ( void );
extern void irq_stub14 ( void );
extern void irq_stub15 ( void );


void irq_init ( void )
{
	// initialize all entries with NULL
	memset( (void*)( kernel_irqs ), 0x00, sizeof(kernel_irqs) );

	// before we can insert any interrupt requests,
	// we need to remap of the first 16 interrupt requests,
	// as by default they are mapped to interrupts 0 to 15 in
	// the interrupt descriptor table
	outbyte( ICW1, PICM_COMMAND_PORT0 );
	outbyte( ICW1, PICS_COMMAND_PORT0 );
	outbyte( ISR_NUM_HANDLERS, PICM_COMMAND_PORT1 );
	outbyte( ISR_NUM_HANDLERS + 0x08, PICS_COMMAND_PORT1 );
	outbyte( 0x04, PICM_COMMAND_PORT1 );
	outbyte( 0x02, PICS_COMMAND_PORT1 );
	outbyte( ICW4, PICM_COMMAND_PORT1 );
	outbyte( ICW4, PICS_COMMAND_PORT1 );
	outbyte( 0x00, PICM_COMMAND_PORT1 );
	outbyte( 0x00, PICS_COMMAND_PORT1 );

	// insert IRQ0 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ0, irq_stub0,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ1 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ1, irq_stub1,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ2 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ2, irq_stub2,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ3 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ3, irq_stub3,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ4 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ4, irq_stub4,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ5 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ5, irq_stub5,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ6 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ6, irq_stub6,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ7 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ7, irq_stub7,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ8 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ8, irq_stub8,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ9 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ9, irq_stub9,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ10 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ10, irq_stub10,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ11 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ11, irq_stub11,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ12 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ12, irq_stub12,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ13 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ13, irq_stub13,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ14 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ14, irq_stub14,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert IRQ15 into the idt table
	idt_set( ISR_NUM_HANDLERS + IRQ15, irq_stub15,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );
}

