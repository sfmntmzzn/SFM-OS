
/* ----------------------------------------------------------------- *
 *
 *   idt_init.c: initializes the interrupt descriptor table
 *               for the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/idt.h>
#include <arch/x86/irq.h>
#include <arch/x86/isr.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>


void idt_init ( void )
{
	// setup pointer and limit for interrupt descriptor table
	kernel_idtr.limit = sizeof(kernel_idt) - 1;
	kernel_idtr.base = (uint32_t)( kernel_idt );

	// initialize all entries with NULL
	memset( (void*)( kernel_idt ), 0x00, sizeof(kernel_idt) );

	// flush the old interrupt entries and load
	// the new ones into the cpu
	idt_flush();
}

