
/* ----------------------------------------------------------------- *
 *
 *   irq_handler.c: implements the top handler that gets called
 *                  for every interrupt request and that then
 *                  itself calls the installed handler.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/irq.h>
#include <arch/x86/isr.h>
#include <sys/io.h>

#include <stddef.h>


#ifndef CDECL
#define CDECL		__attribute__((cdecl))
#endif // CDECL


CDECL void irq_handler ( struct cpu_state* state )
{
	// test whether the interrupt index a valid
	// interrupt request index
	if ( __builtin_expect( state->int_no < ISR_NUM_HANDLERS
		|| ISR_NUM_HANDLERS + IRQ_NUM_HANDLERS <= state->int_no
		, FALSE ) )
	{
		// if not, we should immediately return
		return;
	}

	// get the handler for the specified interrupt index
	void (*handler)( struct cpu_state* )
		= kernel_irqs[ state->int_no - ISR_NUM_HANDLERS ];

	// check if a handler for this interrupt has
	// been installed
	if ( __builtin_expect( handler != NULL, TRUE ) )
	{
		// execute the installed handler
		handler( state );
	}

	// check whether we have one of the top 8
	// interrupt requests
	if ( __builtin_expect( state->int_no - ISR_NUM_HANDLERS >= 8
		, FALSE ) )
	{
		// send a reset signal to the slave PIC
		outbyte( 0xa0, 0x20 );
	}

	// send a reset signal to the master PIC
	outbyte( 0x20, 0x20 );
}

