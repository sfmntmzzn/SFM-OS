
/* ----------------------------------------------------------------- *
 *
 *   serial_init.c: initializes the serial port for debug
 *                  output.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/serial.h>
#include <sys/io.h>

#include <stdint.h>


void serial_init ( uint16_t port, int baud )
{
	// disable interrupts
	outbyte( 0x00, port + SERIAL_IER_OFFSET );

	// set the baud
	unsigned int d = 115200 / baud;
	uint8_t v = inbyte( port + SERIAL_LCR_OFFSET );
	outbyte( v | SERIAL_DLAB_BIT, port + SERIAL_LCR_OFFSET );
	outbyte( d & 0xFF, port + SERIAL_DLL_OFFSET );
	outbyte( ( d >> 8 ) & 0xFF, SERIAL_DLH_OFFSET );
	outbyte( v & ~( SERIAL_DLAB_BIT ), port + SERIAL_LCR_OFFSET );

	// set the remaining values
	outbyte( 0x03, port + SERIAL_LCR_OFFSET );
	outbyte( 0x00, port + SERIAL_FCR_OFFSET );
	outbyte( 0x03, port + SERIAL_MCR_OFFSET );

	// update the serial port variable in the kernel
	kernel_serial_debug_port = port;
}

