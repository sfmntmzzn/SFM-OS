
/* ----------------------------------------------------------------- *
 *
 *   text_init.c: initializes the text mode debug output
 *                of the kernel.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/text.h>

#include <stdint.h>


extern const uint16_t* textbuffer_base;
extern const uint16_t* textbuffer_end;
extern uint8_t textbuffer_color;


void text_init ( void )
{
	// iterate through each 2 bytes in the specified text buffer
	for ( uint16_t* p = (uint16_t*)( textbuffer_base );
		p != (uint16_t*)( textbuffer_end ); ++p )
	{
		// clear the character this 2 bytes represent.
		// The low byte represents the ASCII character,
		// the high byte represents the color value of
		// the character
		*p = 0x0f00;
	}

	// we also need to clear the current color value
	textbuffer_color = 0x0f;
}

