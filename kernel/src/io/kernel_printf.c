
/* ----------------------------------------------------------------- *
 *
 *   kernel_printf.c: imitates the printf function from the
 *                    C standard library for the kernel
 *                    debug output.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>


#define PRINTF_FLAG_DEFAULT		0x00
#define PRINTF_FLAG_LEFT_JUSTIFY	0x01
#define PRINTF_FLAG_FORCE_SIGN		0x02
#define PRINTF_FLAG_PAD_SIGN		0x04
#define PRINTF_FLAG_NUM_PREFIX		0x08
#define PRINTF_FLAG_ZERO_PAD		0x10
#define PRINTF_FLAG_LOWERCASE		0x20

#define PRINTF_LENGTH_HALFHALF		0x00
#define PRINTF_LENGTH_HALF		0x01
#define PRINTF_LENGTH_DEFAULT		0x02
#define PRINTF_LENGTH_LONG		0x03
#define PRINTF_LENGTH_LONGLONG		0x04
#define PRINTF_LENGTH_J			0x05
#define PRINTF_LENGTH_Z			0x06
#define PRINTF_LENGTH_T			0x07
#define PRINTF_LENGTH_L			0x08


static char uppercase_digits[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

static char lowercase_digits[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
};


#define I2STRING(NAME,TYPE) \
static inline char* NAME ( TYPE num, char* buffer, int radix, \
	const unsigned int flags, const int width ) \
{ \
	/* create the temporary string pointer */ \
	/* we will be operating on */ \
	char* p = buffer; \
 \
	/* if num is negative, use it's absolute value */ \
	bool neg = num < 0; \
	if ( __builtin_expect( neg, FALSE ) ) \
	{ \
		/* flip the sign of the number */ \
		num = -num; \
	} \
 \
	/* create a variable to keep track how many */ \
	/* characters are being printed */ \
	int n = 0; \
 \
	/* the string will be constructed in reverse, and */ \
	/* then flipped afterwards */ \
	while ( __builtin_expect( num > 0, TRUE ) ) \
	{ \
		/* did we request uppercase or lowercase digits */ \
		if ( __builtin_expect( flags \
			& PRINTF_FLAG_LOWERCASE, FALSE ) ) \
		{ \
			/* write the last digit from the number */ \
			/* into the buffer */ \
			*p++ = lowercase_digits[ num % radix ]; \
		} \
		else \
		{ \
			/* write the last digit from the number */ \
			/* into the buffer */ \
			*p++ = uppercase_digits[ num % radix ]; \
		} \
 \
		/* strip that last digit from the number by */ \
		/* dividing it by the specified radix */ \
		num /= radix; \
 \
		/* increase the number of printed characters */ \
		++n; \
	} \
 \
	/* check whether we did print a character at all */ \
	if ( __builtin_expect( n == 0, FALSE ) ) \
	{ \
		/* let us just print a zero */ \
		*p++ = '0'; \
		++n; \
	} \
 \
	/* if we have to add a sign to the number, increase the */ \
	/* number of printed characters now */ \
	if ( __builtin_expect( neg || flags & ( PRINTF_FLAG_FORCE_SIGN \
		| PRINTF_FLAG_PAD_SIGN ), FALSE ) ) \
	{ \
		++n; \
	} \
 \
	/* check whether we have to add a prefix to the number */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_NUM_PREFIX, FALSE ) ) \
	{ \
		if ( __builtin_expect( radix == 16, TRUE ) ) \
		{ \
			/* the prefix '0x' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 2 */ \
			n += 2; \
		} \
		else if ( __builtin_expect( radix == 8, TRUE ) ) \
		{ \
			/* the prefix '0' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 1 */ \
			++n; \
		} \
		else if ( __builtin_expect( radix == 2, TRUE ) ) \
		{ \
			/* the prefix '0b' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 2 */ \
			n += 2; \
		} \
	} \
 \
	/* if the zero pad flag is set, append the zeros now */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_ZERO_PAD, FALSE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the '0' character, and increase */ \
			/* the number of printed characters */ \
			*p++ = '0'; \
			++n; \
		} \
	} \
 \
	/* check whether we have to add a prefix to the number */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_NUM_PREFIX, FALSE ) ) \
	{ \
		if ( __builtin_expect( radix == 16, TRUE ) ) \
		{ \
			/* add the prefix '0x' */ \
			*p++ = __builtin_expect( flags \
				& PRINTF_FLAG_LOWERCASE, FALSE ) \
				? 'x' : 'X'; \
			*p++ = '0'; \
		} \
		else if ( __builtin_expect( radix == 8, TRUE ) ) \
		{ \
			/* add the prefix '0' */ \
			*p++ = '0'; \
		} \
		else if ( __builtin_expect( radix == 2, TRUE ) ) \
		{ \
			/* add the prefix '0b' */ \
			*p++ = __builtin_expect( flags \
				& PRINTF_FLAG_LOWERCASE, FALSE ) \
				? 'b' : 'B'; \
			*p++ = '0'; \
		} \
	} \
 \
	/* if we have to add a sign to the number, add it now */ \
	if ( __builtin_expect( neg, FALSE ) ) \
	{ \
		/* append the '-' sign */ \
		*p++ = '-'; \
	} \
	else if ( __builtin_expect( flags & PRINTF_FLAG_FORCE_SIGN, FALSE ) ) \
	{ \
		/* append the '+' sign */ \
		*p++ = '+'; \
	} \
	else if ( __builtin_expect( flags & PRINTF_FLAG_PAD_SIGN, FALSE ) ) \
	{ \
		/* append the sign padding */ \
		*p++ = ' '; \
	} \
 \
	/* if the left justification flag is set, append the spaces now */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_LEFT_JUSTIFY, FALSE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the space character, and increase */ \
			/* the number of printed characters */ \
			*p++ = ' '; \
			++n; \
		} \
	} \
 \
	/* flip the digits to right order */ \
	for ( char* a = buffer, *b = p - 1; a < b; ) \
	{ \
		/* swap the two characters pointed to by a and b */ \
		char t = *b; \
		*b-- = *a; \
		*a++ = t; \
	} \
 \
	/* if the left justification flag is not set, append */ \
	/* the spaces now */ \
	if ( __builtin_expect( \
		!( flags & PRINTF_FLAG_LEFT_JUSTIFY ), TRUE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the space character, and increase */ \
			/* the number of printed characters */ \
			*p++ = ' '; \
			++n; \
		} \
	} \
 \
	/* append the NUL character to terminate the string */ \
	*p++ = '\0'; \
 \
	/* return the pointer to the created string */ \
	return buffer; \
}

#define U2STRING(NAME,TYPE) \
static inline char* NAME ( TYPE num, char* buffer, int radix, \
	const unsigned int flags, const int width ) \
{ \
	/* create the temporary string pointer */ \
	/* we will be operating on */ \
	char* p = buffer; \
 \
	/* create a variable to keep track how many */ \
	/* characters are being printed */ \
	int n = 0; \
 \
	/* the string will be constructed in reverse, and */ \
	/* then flipped afterwards */ \
	while ( __builtin_expect( num > 0, TRUE ) ) \
	{ \
		/* did we request uppercase or lowercase digits */ \
		if ( __builtin_expect( flags \
			& PRINTF_FLAG_LOWERCASE, FALSE ) ) \
		{ \
			/* write the last digit from the number */ \
			/* into the buffer */ \
			*p++ = lowercase_digits[ num % radix ]; \
		} \
		else \
		{ \
			/* write the last digit from the number */ \
			/* into the buffer */ \
			*p++ = uppercase_digits[ num % radix ]; \
		} \
 \
		/* strip that last digit from the number by */ \
		/* dividing it by the specified radix */ \
		num /= radix; \
 \
		/* increase the number of printed characters */ \
		++n; \
	} \
 \
	/* check whether we did print a character at all */ \
	if ( __builtin_expect( n == 0, FALSE ) ) \
	{ \
		/* let us just print a zero */ \
		*p++ = '0'; \
		++n; \
	} \
 \
	/* if we have to add a sign to the number, increase the */ \
	/* number of printed characters now */ \
	if ( __builtin_expect( flags & ( PRINTF_FLAG_FORCE_SIGN \
		| PRINTF_FLAG_PAD_SIGN ), FALSE ) ) \
	{ \
		++n; \
	} \
 \
	/* check whether we have to add a prefix to the number */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_NUM_PREFIX, FALSE ) ) \
	{ \
		if ( __builtin_expect( radix == 16, TRUE ) ) \
		{ \
			/* the prefix '0x' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 2 */ \
			n += 2; \
		} \
		else if ( __builtin_expect( radix == 8, TRUE ) ) \
		{ \
			/* the prefix '0' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 1 */ \
			++n; \
		} \
		else if ( __builtin_expect( radix == 2, TRUE ) ) \
		{ \
			/* the prefix '0b' has to be added. Therefore, */ \
			/* increase the number of printed characters */ \
			/* by 2 */ \
			n += 2; \
		} \
	} \
 \
	/* if the zero pad flag is set, append the zeros now */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_ZERO_PAD, FALSE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the '0' character, and increase */ \
			/* the number of printed characters */ \
			*p++ = '0'; \
			++n; \
		} \
	} \
 \
	/* check whether we have to add a prefix to the number */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_NUM_PREFIX, FALSE ) ) \
	{ \
		if ( __builtin_expect( radix == 16, TRUE ) ) \
		{ \
			/* add the prefix '0x' */ \
			*p++ = __builtin_expect( flags \
				& PRINTF_FLAG_LOWERCASE, FALSE ) \
				? 'x' : 'X'; \
			*p++ = '0'; \
		} \
		else if ( __builtin_expect( radix == 8, TRUE ) ) \
		{ \
			/* add the prefix '0' */ \
			*p++ = '0'; \
		} \
		else if ( __builtin_expect( radix == 2, TRUE ) ) \
		{ \
			/* add the prefix '0b' */ \
			*p++ = __builtin_expect( flags \
				& PRINTF_FLAG_LOWERCASE, FALSE ) \
				? 'b' : 'B'; \
			*p++ = '0'; \
		} \
	} \
 \
	/* if we have to add a sign to the number, add it now */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_FORCE_SIGN, FALSE ) ) \
	{ \
		/* append the '+' sign */ \
		*p++ = '+'; \
	} \
	else if ( __builtin_expect( flags & PRINTF_FLAG_PAD_SIGN, FALSE ) ) \
	{ \
		/* append the sign padding */ \
		*p++ = ' '; \
	} \
\
	/* if the left justification flag is set, append the spaces now */ \
	if ( __builtin_expect( flags & PRINTF_FLAG_LEFT_JUSTIFY, FALSE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the space character, and increase */ \
			/* the number of printed characters */ \
			*p++ = ' '; \
			++n; \
		} \
	} \
 \
	/* flip the digits to right order */ \
	for ( char* a = buffer, *b = p - 1; a < b; ) \
	{ \
		/* swap the two characters pointed to by a and b */ \
		char t = *b; \
		*b-- = *a; \
		*a++ = t; \
	} \
 \
	/* if the left justification flag is not set, append */ \
	/* the spaces now */ \
	if ( __builtin_expect( \
		!( flags & PRINTF_FLAG_LEFT_JUSTIFY ), TRUE ) ) \
	{ \
		while ( __builtin_expect( n < width, TRUE ) ) \
		{ \
			/* append the space character, and increase */ \
			/* the number of printed characters */ \
			*p++ = ' '; \
			++n; \
		} \
	} \
 \
	/* append the NUL character to terminate the string */ \
	*p++ = '\0'; \
 \
	/* return the pointer to the created string */ \
	return buffer; \
}


I2STRING( i2string, signed int )
I2STRING( l2string, signed long int )
I2STRING( ll2string, signed long long int )

U2STRING( u2string, unsigned int )
U2STRING( ul2string, unsigned long int )
U2STRING( ull2string, unsigned long long int )


static inline int handle_signed ( va_list* const args, const unsigned int radix,
	const unsigned int flags, const int width, const unsigned int length )
{
	// create a character buffer, where the integer string
	// should be written in. 127+1 characters should be enough
	// space even when large widths are beeing specified
	char buf[ 128 ];

	// read the passed argument into the buffer
	switch ( length )
	{
	case PRINTF_LENGTH_HALFHALF:
		// read a byte into the buffer
		i2string( (char)( va_arg( *args, int ) ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_HALF:
		// read a short int into the buffer
		i2string( (short)( va_arg( *args, int ) ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_DEFAULT:
		// read a int into the buffer
		i2string( va_arg( *args, int ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_LONG:
		// read a long int into the buffer
		l2string( va_arg( *args, long int ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_LONGLONG:
		// read a long long int into the buffer
		ll2string( va_arg( *args, long long int ),
			buf, radix, flags, width );
		break;
	}

	// get the length of the string we are about to print
	int len = strlen( buf );

	// call `kernel_putstring' to print that string
	kernel_putstring( buf );

	// return the amount of characters printed
	return len;
}


static inline int handle_unsigned ( va_list* const args,
	const unsigned int radix, const unsigned int flags,
	const int width, const unsigned int length )
{
	// create a character buffer, where the integer string
	// should be written in. 127+1 characters should be enough
	// space even when large widths are beeing specified
	char buf[ 128 ];

	// read the passed argument into the buffer
	switch ( length )
	{
	case PRINTF_LENGTH_HALFHALF:
		// read a unsigned byte into the buffer
		u2string( (unsigned char)( va_arg( *args, unsigned int ) ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_HALF:
		// read a unsigned short int into the buffer
		u2string( (unsigned short)( va_arg( *args, unsigned int ) ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_DEFAULT:
		// read a unsigned int into the buffer
		u2string( va_arg( *args, unsigned int ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_LONG:
		// read a unsigned long int into the buffer
		ul2string( va_arg( *args, unsigned long int ),
			buf, radix, flags, width );
		break;
	case PRINTF_LENGTH_LONGLONG:
		// read a unsigned long long int into the buffer
		ull2string( va_arg( *args, unsigned long long int ),
			buf, radix, flags, width );
		break;
	}

	// get the length of the string we are about to print
	int len = strlen( buf );

	// call `kernel_putstring' to print that string
	kernel_putstring( buf );

	// return the amount of characters printed
	return len;
}


static inline int handle_character ( va_list* const args,
	const unsigned int flags, const int width )
{
	// retrieve the string pointer from the argument list
	char c = (char)( va_arg( *args, int ) );

	// if we should left justify and a width has been
	// specified, print the missing spaces immediately
	if ( __builtin_expect( flags & PRINTF_FLAG_LEFT_JUSTIFY
		&& width > 0, FALSE ) )
	{
		for ( int n = 0; n < width - 1; ++n )
		{
			// print a space character
			kernel_putchar( ' ' );
		}
	}

	// call `kernel_putchar' to push that character
	kernel_putchar( c );

	// if we should right justify and a width has been
	// specified, print the missing spaces
	if ( __builtin_expect( !( flags & PRINTF_FLAG_LEFT_JUSTIFY )
		&& width > 0, FALSE ) )
	{
		for ( int n = 0; n < width - 1; ++n )
		{
			// print a space character
			kernel_putchar( ' ' );
		}
	}

	// return the amount of characters printed
	return __builtin_expect( width > 0, FALSE ) ? width : 1;
}


static int handle_string ( va_list* const args,
	const unsigned int flags, const int width )
{
	// retrieve the string pointer from the argument
	// list
	const char* s = va_arg( *args, const char* );

	// get the length of the string we are about to print
	int len = strlen( s );

	// if we should left justify and a width has been
	// specified, print the missing spaces immediately
	if ( __builtin_expect( flags & PRINTF_FLAG_LEFT_JUSTIFY
		&& width > 0, FALSE ) )
	{
		for ( int n = 0; n < width - len; ++n )
		{
			// print a space character
			kernel_putchar( ' ' );
		}
	}

	// call `kernel_putstring' to print that string
	kernel_putstring( s );

	// if we should right justify and a width has been
	// specified, print the missing spaces
	if ( __builtin_expect( !( flags & PRINTF_FLAG_LEFT_JUSTIFY )
		&& width > 0, FALSE ) )
	{
		for ( int n = 0; n < width - len; ++n )
		{
			// print a space character
			kernel_putchar( ' ' );
		}
	}

	// return the amount of characters printed
	return __builtin_expect( width > 0, FALSE ) ? width : len;
}


static inline char* handle_format_request ( va_list* const args,
	const char* p, int* count )
{
	// create all required temporal variables for the
	// format calculation
	char* c = (char*)( p );
	unsigned int flags = PRINTF_FLAG_DEFAULT;
	unsigned int length = PRINTF_LENGTH_DEFAULT;
	int width = 0, precision = 0;

	// parse the flag sub-specifiers, in case some
	// have been specified
	for ( bool parse_flags = true; parse_flags; )
	{
		// switch through the possible flag
		// sub-specifiers
		switch ( *c++ )
		{
		case '-':
			// the left justify flag was requested
			flags |= PRINTF_FLAG_LEFT_JUSTIFY;
			break;
		case '+':
			// the force sign flag was requested
			flags |= PRINTF_FLAG_FORCE_SIGN;
			break;
		case ' ':
			// the pad sign flag was requested
			flags |= PRINTF_FLAG_PAD_SIGN;
			break;
		case '#':
			// the number prefix flag was requested
			flags |= PRINTF_FLAG_NUM_PREFIX;
			break;
		case '0':
			// the zero pad flag was requested
			flags |= PRINTF_FLAG_ZERO_PAD;
			break;
		default:
			// the next character is not a flag
			// character anymore. Therefore, stop
			// parsing flag specifiers
			parse_flags = false;
			--c;
			break;
		}
	}

	// parse the width sub-specifier, in case one has
	// been provided. We can expect either a number
	// or '*'
	if ( __builtin_expect( *c >= '1' && *c <= '9', FALSE ) )
	{
		// set the first digit of the width variable
		width = *c++ - '0';

		// parse the following digits of the
		// specified number
		while ( __builtin_expect( *c >= '0' && *c <= '9', TRUE ) )
		{
			// add the parsed digit to the
			// width variable
			width = ( width * 10 ) + ( *c++ - '0' );
		}
	}
	else if ( __builtin_expect( *c == '*', FALSE ) )
	{
		// read the width out of the argument list
		width = va_arg( *args, int );
		++c;
	}

	// parse the precision sub-specifier, in case one
	// has been provided
	if ( __builtin_expect( *c == '.', FALSE ) )
	{
		// First, move to the next character
		++c;

		// we can expect either a number or '*'.
		if ( __builtin_expect( *c >= '1' && *c <= '9', TRUE ) )
		{
			// set the first digit of the width variable
			precision = *c++ - '0';

			// parse the following digits of the
			// specified number
			while ( __builtin_expect(
				*c >= '0' && *c <= '9', TRUE ) )
			{
				// add the parsed digit to the
				// width variable
				precision = ( precision * 10 ) + *c++ - '0';
			}
		}
		else if ( __builtin_expect( *c == '*', FALSE ) )
		{
			// read the width out of the argument list
			width = va_arg( *args, int );
			++c;
		}
		else
		{
			// neither a number or '*' was found. We have
			// to return now
			return (char*)( p );
		}
	}

	// parse the length sub-specifier, in case one
	// has been provided
	if ( __builtin_expect( *c == 'h', FALSE ) )
	{
		if ( __builtin_expect( *++c == 'h', FALSE ) )
		{
			// double half length has been specified
			length = PRINTF_LENGTH_HALFHALF;
			++c;
		}
		else
		{
			// single half length has been specified
			length = PRINTF_LENGTH_HALF;
		}
	}
	else if ( __builtin_expect( *c == 'l', FALSE ) )
	{
		if ( __builtin_expect( *++c == 'l', FALSE ) )
		{
			// double long length has been specified
			length = PRINTF_LENGTH_LONGLONG;
			++c;
		}
		else
		{
			// single long length has been specified
			length = PRINTF_LENGTH_LONG;
		}
	}
	else if ( __builtin_expect( *c == 'j', FALSE ) )
	{
		// max int length has been specified
		length = PRINTF_LENGTH_J;
		++c;
	}
	else if ( __builtin_expect( *c == 'z', FALSE ) )
	{
		// size length has been specified
		length = PRINTF_LENGTH_Z;
		++c;
	}
	else if ( __builtin_expect( *c == 't', FALSE ) )
	{
		// pointer difference length has been specified
		length = PRINTF_LENGTH_T;
		++c;
	}
	else if ( __builtin_expect( *c == 'L', FALSE ) )
	{
		// long double floating point length has been specified
		length = PRINTF_LENGTH_L;
		++c;
	}

	// switch through the possible format specifiers
	switch ( *c++ )
	{
	case 'd':
	case 'i':
		// printing of a signed decimal integer
		// was requested
		*count += handle_signed( args, 10U, flags, width, length );
		break;
	case 'u':
		// printing of a unsigned decimal integer
		// was requested
		*count += handle_unsigned( args, 10U, flags, width, length );
		break;
	case 'o':
		// printing of a unsigned octal integer
		// was requested
		*count += handle_unsigned( args, 8U, flags, width, length );
		break;
	case 'x':
		// printing of a lowercase unsigned
		// hexadecimal integer was requested
		flags |= PRINTF_FLAG_LOWERCASE;
		__attribute__((fallthrough));
	case 'X':
		// printing of a uppercase unsigned
		// hexadecimal integer was requested
		*count += handle_unsigned( args, 16U, flags, width, length );
		break;
	case 'f':
		// printing of a lowercase decimal
		// floating point number was requested
		__attribute__((fallthrough));
	case 'F':
		// printing of a uppercase decimal
		// floating point number was requested
		break;
	case 'e':
		// printing of a lowercase scientific
		// notation was requested
		__attribute__((fallthrough));
	case 'E':
		// printing of a uppercase scientific
		// notation was requested
		break;
	case 'g':
		// printing of the lowercase shortest
		// representation was requested
		__attribute__((fallthrough));
	case 'G':
		// printing of the uppercase shortest
		// representation was requested
		break;
	case 'a':
		// printing of a lowercase hexadecimal
		// floating point number was requested
		__attribute__((fallthrough));
	case 'A':
		// printing of a uppercase hexadecimal
		// floating point number was requested
		break;
	case 'c':
		// printing of a character was requested
		*count += handle_character( args, flags, width );
		break;
	case 's':
		// printing of a string was requested
		*count += handle_string( args, flags, width );
		break;
	case 'p':
		// printing of a pointer address was requested
		flags |= PRINTF_FLAG_LOWERCASE;
		*count += handle_unsigned( args, 16, flags, width, length );
		break;
	case 'n':
		// number of written characters requested
		*( va_arg( *args, int* ) ) = *count;
		break;
	case '%':
		// printing of the '%' character was requested
		kernel_putchar( '%' );
		++( *count );
		break;
	default:
		// there was no valid specifier,
		// we will do nothing
		return (char*)( p );
	}

	// return the new pointer to the next
	// character that was not yet parsed
	return c;
}


int kernel_printf ( const char* fmt, ... )
{
	// create the count variable which holds the
	// amount of printed characters
	int count;

	// initialize the variadic argument list
	va_list args;
	va_start( args, fmt );

	// create the iteration variable for each
	// character in the passed string
	char* c = (char*)( fmt );

	// iterate through each character in the
	// format string
	for ( count = 0; *c; )
	{
		// if the current character is a '%',
		// we have to parse a format specifier
		if ( *c == '%' )
		{
			// call the helper function to handle it
			c = handle_format_request( &args, ++c, &count );
			continue;
		}

		// otherwise, just print the character
		// and move on with the loop
		kernel_putchar( *c++ );

		// increase the character count
		++count;
	}

	// release the variadic argument list
	va_end( args );

	// return the amount of printed characters
	return count;
}

