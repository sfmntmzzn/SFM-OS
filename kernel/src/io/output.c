
/* ----------------------------------------------------------------- *
 *
 *   output.c: variables used for the general debug output
 *             handling of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>

#include <stdint.h>


int kernel_active_debug_mode = 0x00;

