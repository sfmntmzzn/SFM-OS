
/* ----------------------------------------------------------------- *
 *
 *   serial.c: variables used for the serial debug output of
 *             the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/serial.h>

#include <stdint.h>


uint16_t kernel_serial_debug_port = 0x0000;

