
/* ----------------------------------------------------------------- *
 *
 *   pageframe_deallocate.c: frees the specified number of
 *                           consecutive page frames.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/pageframe.h>

#include <stddef.h>


static void pageframe_mark_free ( int page, size_t num,
	const struct pageframe_buddy* buddy )
{
	// loop `num' times
	for ( int i = page / (1<<( buddy->order ));
		i < (int)( page + num ); ++i )
	{
		// mark that entry as used
		( buddy->map )[ i ] = PAGEFRAME_FREE;
	}
}


int pageframe_deallocate ( int page, size_t num )
{
	// check whether valid pageframe index and deallocation
	// amount were passed
	if ( __builtin_expect( 0 > page || !num, FALSE ) )
	{
		// if not, return
		return -1;
	}

#ifdef KERNEL_SAFETY
	// check whether the first buddy in the system has order 0
	if ( __builtin_expect( kernel_pageframe_buddies[ 0 ].order
		!= 0, FALSE ) )
	{
		return -1;
	}
#endif // KERNEL_SAFETY

	// iterate through the pageframe buddies
	for ( int i = 0; i != kernel_pageframe_buddy_count; ++i )
	{
		// retrieve the buddy parameters
		int o = kernel_pageframe_buddies[ i ].order;

		// check whether it's order is greater than `num'
		if ( __builtin_expect( (1<<o) >= (int)( num ), FALSE ) )
		{
			// update `num'
			num = (1<<o);

			// break
			goto next;
		}
	}

	// there is no appropriate buddy in the system,
	// return
	return -1;

next:
	// mark the pageframes as free in the order0 buddy
	pageframe_mark_free( page, num, &( kernel_pageframe_buddies[ 0 ] ) );

	// variables
	int old_o = 0;
	uint8_t* old_m = kernel_pageframe_buddies[ 0 ].map;

	// iterate through the rest of the pageframe buddies
	for ( int i = 1; i != kernel_pageframe_buddy_count; ++i )
	{
		// retrieve the buddy parameters
		int o = kernel_pageframe_buddies[ i ].order;
		uint8_t* m = kernel_pageframe_buddies[ i ].map;

		// get the first aligned pageframe address
		int _page = page & -( (1<<o) );

		// iterate through all buddy entries important
		// for the requested pageframe block
		for ( int j = _page; j < (int)( page + num ); j += (1<<o) )
		{
			// variable for the state of this block
			uint8_t s = PAGEFRAME_FREE;

			// iterate through the important buddy entries
			// in the buddy one level lower than the current
			for ( int k = j; k < j + (1<<o); k += (1<<old_o) )
			{
				// check whether that buddy entry indicates
				// that the pageframe block is used
				if ( __builtin_expect(
					old_m[ k / (1<<old_o) ]
					== PAGEFRAME_USED, FALSE ) )
				{
					// update `s'
					s = PAGEFRAME_USED;
				}
			}

			// write `s' into the buddy entry
			m[ j / (1<<o) ] = s;
		}

		// update `old_o' and `old_m'
		old_o = o;
		old_m = m;
	}

	// return
	return 0;
}

