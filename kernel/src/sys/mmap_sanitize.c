
/* ----------------------------------------------------------------- *
 *
 *   mmap_sanitize.c: sanitizes the MMAP structure of
 *                    the kernel. This implementation is
 *                    greatly inspired by the one used
 *                    by the linux kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/mmap.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


struct change_point
{
	struct mmap_entry* entry;
	uint64_t addr;
};


static struct mmap_entry mmap_temp[ MAX_MMAP_ENTRIES ];


static int change_point_compare ( const void* a, const void* b )
{
	// create local variables with their types
	const struct change_point* _a = (const struct change_point*)( a );
	const struct change_point* _b = (const struct change_point*)( b );

	// test if the addresses are different
	if ( __builtin_expect( _a->addr != _b->addr, TRUE ) )
	{
		// if so, return their difference
		return ( _a->addr > _b->addr ) ? 1 : -1;
	}

	// else prioritize start pointers
	return ( _a->addr != _a->entry->base )
		- ( _b->addr != _b->entry->base );
}


void mmap_sanitize ( void )
{
	// create the local change point array this
	// algorithm is going to be operating on
	struct change_point change_points[ MAX_MMAP_ENTRIES * 2 ];
	struct change_point* change_point_end = change_points;

	// iterate over all current entries in the MMAP
	// structure
	for ( struct mmap_entry* p = kernel_mmap.start;
		p < kernel_mmap.end; ++p )
	{
		// insert the base change point into the
		// pointer array
		change_point_end->entry = p;
		change_point_end->addr = p->base;
		++change_point_end;

		// next, insert the end change point into
		// the pointer array
		change_point_end->entry = p;
		change_point_end->addr = p->base + p->length;
		++change_point_end;
	}

	// sort the change points using `qsort' from
	// the C standard library
	qsort( (void*)( change_points ), change_point_end - change_points,
		sizeof(struct change_point), change_point_compare );

	// create the local overlap list this algorithm
	// is going to be operating on
	struct mmap_entry* overlaps[ MAX_MMAP_ENTRIES ];
	struct mmap_entry** overlaps_end = overlaps;

	// create the local end pointer for the temporary
	// MMAP structure of this function
	struct mmap_entry* mmap_temp_current = mmap_temp;

	// create the variable indicating what the address
	// and type of the entry from the previous
	// iteration were
	uint64_t addr_old = 0x00000000;
	uint32_t type_old = MMAP_TYPE_UNDEFINED;

	// now iterate over the sorted change points
	for ( struct change_point* p = change_points;
		p < change_point_end; ++p )
	{
		// test whether we have a start change
		// point
		if ( __builtin_expect( p->addr == p->entry->base, TRUE ) )
		{
			// if we do, append the change point
			// to the overlap list
			*overlaps_end++ = p->entry;
		}
		else
		{
			// else, decrease `overlaps_end'
			--overlaps_end;

			// remove the start change point from
			// the list by moving the last element to its
			// position
			for ( struct mmap_entry** q = overlaps;
				q < overlaps_end; ++q )
			{
				// if this is the corresponding start
				// change point, move the last element
				// here
				if ( __builtin_expect(
					*q == p->entry, FALSE ) )
				{
					// update `q's value with the
					// one from the last element
					*q = *overlaps_end;
				}
			}
		}

		// get the maximum type from all entries in the
		// overlap list
		uint32_t t = MMAP_TYPE_UNDEFINED;
		for ( struct mmap_entry** q = overlaps; q < overlaps_end; ++q )
		{
			// check whether the type of `q' is
			// bigger than the current `t'
			if ( __builtin_expect( ( *q )->type > t, FALSE ) )
			{
				// update `t'
				t = ( *q )->type;
			}
		}

		// check whether we are facing a new region in the
		// memory map
		if ( __builtin_expect( t != type_old, FALSE ) )
		{
			// check whether we actually have a
			// old type
			if ( __builtin_expect(
				type_old != MMAP_TYPE_UNDEFINED, TRUE ) )
			{
				// finish the current entry of the
				// new temporary mmap structure by
				// setting its length
				mmap_temp_current->length =
					p->addr - addr_old;

				// test if the length was greater than
				// zero
				if ( __builtin_expect(
					mmap_temp_current->length != 0, TRUE ) )
				{
					// increase `mmap_temp_current'
					++mmap_temp_current;
				}
			}

			// check whether we actually have a
			// old type
			if ( __builtin_expect(
				t != MMAP_TYPE_UNDEFINED, TRUE ) )
			{
				// start a new entry by setting its
				// base address and type
				mmap_temp_current->base = p->addr;
				mmap_temp_current->type = t;
				mmap_temp_current->zero = 0x00000000;

				// update `addr_old'
				addr_old = p->addr;
			}

			// update `type_old'
			type_old = t;
		}
	}

	// iterate over all entries in the MMAP structure, and
	// search for gaps between two entries. If there are
	// gaps, insert a entry of type `undefined' for these
	// regions
	for ( struct mmap_entry* p = mmap_temp + 1;
		p < mmap_temp_current; ++p )
	{
		// let `q' point to the previous entry
		struct mmap_entry* q = p - 1;

		// check whether there is a gap between the
		// two entries
		if ( __builtin_expect(
			q->base + q->length < p->base, FALSE ) )
		{
			// move all following entries back by 1
			memmove( (void*)( p + 1 ), (void*)( p ),
				(uintptr_t)( mmap_temp_current )
				- (uintptr_t)( p ) );

			// initialize the gap entry
			p->base = q->base + q->length;
			p->length = ( p + 1 )->base - p->base;
			p->type = MMAP_TYPE_UNDEFINED;

			// increase `p' and `mmap_temp_current'
			++mmap_temp_current;
			++p;
		}
	}

	// copy the new contructed mmap structure into the
	// official location in the kernel memory
	size_t n = mmap_temp_current - mmap_temp;
	memcpy( (void*)( kernel_mmap.start ), (void*)( mmap_temp ),
		n * sizeof(struct mmap_entry) );
	kernel_mmap.end = kernel_mmap.start + n;

#ifdef KERNEL_SAFETY
	// mark that the MMAP structure is sanitized in
	// it's current state
	kernel_mmap.sanitized = true;
#endif // KERNEL_SAFETY

	// return
	return;
}


