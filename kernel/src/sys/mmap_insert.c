
/* ----------------------------------------------------------------- *
 *
 *   mmap_insert.c: insert a entry into the MMAP structure
 *                  of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/mmap.h>

#include <stdbool.h>
#include <stddef.h>


void mmap_insert ( struct mmap_entry* entry )
{
	// check whether there is still enough space for
	// another entry in the structure
	if ( __builtin_expect( kernel_mmap.end - kernel_mmap.start
		>= MAX_MMAP_ENTRIES, FALSE ) )
	{
		// return
		return;
	}

	// copy the entry to the end of the kernel
	// MMAP structure
	*( kernel_mmap.end )++ = *entry;

#ifdef KERNEL_SAFETY
	// mark the changes were made to the MMAP structure,
	// so that it will most likely not be sanitized
	// anymore
	kernel_mmap.sanitized = false;
#endif // KERNEL_SAFETY
}

