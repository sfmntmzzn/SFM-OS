
/* ----------------------------------------------------------------- *
 *
 *   mmap.c: variables for the MMAP structure of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/mmap.h>

#include <stdbool.h>
#include <stddef.h>


static struct mmap_entry __kernel_mmap[ MAX_MMAP_ENTRIES ];


struct mmap_info kernel_mmap = 
{
	.start = __kernel_mmap,
	.end = __kernel_mmap,
#ifdef KERNEL_SAFETY
	.sanitized = false,
#endif // KERNEL_SAFETY
};

