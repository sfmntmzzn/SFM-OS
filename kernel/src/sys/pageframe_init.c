
/* ----------------------------------------------------------------- *
 *
 *   pageframe_init.c: initializes the buddy allocation system.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/mmap.h>
#include <sys/pageframe.h>

#include <string.h>


#define ROUND_DOWN(x)	( (x) & ( -PAGE_SIZE ) )
#define ROUND_UP(x)	ROUND_DOWN( (x) + ( PAGE_SIZE - 1 ) )


void pageframe_init ( void )
{
	// iterate through all pageframe buddies, and mark
	// all pages as used
	for ( int i = 0; i != kernel_pageframe_buddy_count; ++i )
	{
		// retrieve the buddy parameters
		int o = kernel_pageframe_buddies[ i ].order;
		uint8_t* a = kernel_pageframe_buddies[ i ].map;

		// mark all entries for this buddy as used
		memset( (void*)( a ), PAGEFRAME_USED, BUDDY_ARRSIZE( o ) );
	}

	// now, iterate through the MMAP entries of
	// the kernel, and update the pages that are
	// available according to the MMAP structure
	for ( struct mmap_entry* e = kernel_mmap.start;
		e != kernel_mmap.end; ++e )
	{
		// check if it is a MMAP entry of
		// type `FREE'
		if ( __builtin_expect( e->type != MMAP_TYPE_FREE, TRUE ) )
		{
			// continue
			continue;
		}

		// get the start and end address of the
		// page frame block
		int p = ROUND_UP( e->base ) / PAGE_SIZE;
		int q = ROUND_DOWN( e->base + e->length ) / PAGE_SIZE;

		// iterate over all pageframes in that area
		for ( int i = p; i < q; ++i )
		{
			// mark the pageframe as free
			pageframe_deallocate( i, 1 );
		}
	}
}

