
/* ----------------------------------------------------------------- *
 *
 *   meminfo.c: few variables for the memory info passed by
 *              the bootloader.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/meminfo.h>

#include <stddef.h>


struct memory_info kernel_meminfo = { 0x00000000, 0x00000000 };


