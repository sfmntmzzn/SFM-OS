
/* ----------------------------------------------------------------- *
 *
 *   outbyte.c: write a single byte into the specified port.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/io.h>

#include <stdint.h>



void outbyte ( uint8_t value, uint16_t port )
{
	// just use the x86 `outb' instruction
	__asm__ __volatile__ ( "outb %b0, %w1" :: "a"(value), "Nd"(port) );
}

