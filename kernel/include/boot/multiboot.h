
/* ----------------------------------------------------------------- *
 *
 *   multiboot.h: structure definitions for the implementation
 *                of the multiboot specification.
 *                For more information, see:
 *
 *   https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_BOOT_MULTIBOOT_HEADER
#define KERNEL_BOOT_MULTIBOOT_HEADER


#ifndef PACKED
#define PACKED		__attribute__((packed))
#endif // PACKED


typedef __UINT8_TYPE__ multiboot_uint8_t;
typedef __UINT16_TYPE__ multiboot_uint16_t;
typedef __UINT32_TYPE__ multiboot_uint32_t;
typedef __UINT64_TYPE__ multiboot_uint64_t;


#define MULTIBOOT_INFO_MAGIC			0x36d76289

#define MULTIBOOT_TAG_END			0
#define MULTIBOOT_TAG_CMDLINE			1
#define MULTIBOOT_TAG_BOOTLOADER		2
#define MULTIBOOT_TAG_MODULE			3
#define MULTIBOOT_TAG_MEMORY_INFO		4
#define MULTIBOOT_TAG_BOOT_DEVICE		5
#define MULTIBOOT_TAG_MMAP			6
#define MULTIBOOT_TAG_VBE			7
#define MULTIBOOT_TAG_FRAMEBUFFER		8
#define MULTIBOOT_TAG_ELF_SECTIONS		9
#define MULTIBOOT_TAG_APM			10
#define MULTIBOOT_TAG_EFI32			11
#define MULTIBOOT_TAG_EFI64			12
#define MULTIBOOT_TAG_SMBIOS			13
#define MULTIBOOT_TAG_ACPI_OLD			14
#define MULTIBOOT_TAG_ACPI_NEW			15
#define MULTIBOOT_TAG_NETWORK			16
#define MULTIBOOT_TAG_EFI_MMAP			17
#define MULTIBOOT_TAG_EFI_NT			18
#define MULTIBOOT_TAG_EFI32_IH			19
#define MULTIBOOT_TAG_EFI64_IH			20
#define MULTIBOOT_TAG_LOAD_BASE			21

#define MULTIBOOT_MMAP_RAM			1
#define MULTIBOOT_MMAP_ACPI			3
#define MULTIBOOT_MMAP_RESERVED			4
#define MULTIBOOT_MMAP_OCCUPIED			5

#define MULTIBOOT_FRAMEBUFFER_INDEXED		0
#define MULTIBOOT_FRAMEBUFFER_DIRECT		1
#define MULTIBOOT_FRAMEBUFFER_EGA		2


struct multiboot_info_tag
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
} PACKED;


struct multiboot_info_tag_memory
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t mem_lower;
	multiboot_uint32_t mem_upper;
} PACKED;


struct multiboot_info_tag_bios_device
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t biosdev;
	multiboot_uint32_t partition;
	multiboot_uint32_t sub_partition;
} PACKED;


struct multiboot_info_tag_command_line
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint8_t string[0];
} PACKED;


struct multiboot_info_tag_modules
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t mod_start;
	multiboot_uint32_t mod_end;
	multiboot_uint8_t string[0];
} PACKED;


struct multiboot_info_tag_elf_symbols
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint16_t num;
	multiboot_uint16_t entsize;
	multiboot_uint16_t shndx;
	multiboot_uint16_t reserved;
	multiboot_uint8_t section_headers[0];
} PACKED;


struct multiboot_mmap_entry
{
	multiboot_uint64_t base_addr;
	multiboot_uint64_t length;
	multiboot_uint32_t type;
	multiboot_uint32_t reserved;
} PACKED;


struct multiboot_info_tag_mmap
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t entry_size;
	multiboot_uint32_t entry_version;
	struct multiboot_mmap_entry entries[0];
} PACKED;


struct multiboot_info_tag_bootloader_name
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint8_t string[0];
} PACKED;


struct multiboot_info_tag_vbe_info
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint16_t vbe_mode;
	multiboot_uint16_t vbe_interface_seg;
	multiboot_uint16_t vbe_interface_off;
	multiboot_uint16_t vbe_interface_len;
	multiboot_uint8_t vbe_control_info[512];
	multiboot_uint8_t vbe_mode_info[256];
} PACKED;


struct multiboot_color
{
	multiboot_uint8_t red_value;
	multiboot_uint8_t green_value;
	multiboot_uint8_t blue_value;
} PACKED;


struct multiboot_indexed_palette
{
	multiboot_uint32_t framebuffer_palette_size;
	struct multiboot_color framebuffer_palette[0];
} PACKED;


struct multiboot_direct_palette
{
	multiboot_uint8_t framebuffer_red_field_position;
	multiboot_uint8_t framebuffer_red_mask_size;
	multiboot_uint8_t framebuffer_green_field_position;
	multiboot_uint8_t framebuffer_green_mask_size;
	multiboot_uint8_t framebuffer_blue_field_position;
	multiboot_uint8_t framebuffer_blue_mask_size;
} PACKED;


struct multiboot_info_tag_framebuffer_info
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint64_t framebuffer_addr;
	multiboot_uint32_t framebuffer_pitch;
	multiboot_uint32_t framebuffer_width;
	multiboot_uint32_t framebuffer_height;
	multiboot_uint8_t framebuffer_bpp;
	multiboot_uint8_t framebuffer_type;
	multiboot_uint8_t reserved;
	multiboot_uint8_t color_info[0];
} PACKED;


struct multiboot_info_tag_efi32_system_table
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t pointer;
} PACKED;


struct multiboot_info_tag_efi64_system_table
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint64_t pointer;
} PACKED;


struct multiboot_info_tag_smbios_table
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint8_t major;
	multiboot_uint8_t minor;
	multiboot_uint8_t reserved[6];
	multiboot_uint8_t smbios_tables[0];
} PACKED;


struct multiboot_info_tag_acpi
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint8_t rsdp[0];
} PACKED;


struct multiboot_info_tag_network
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint8_t dhcp[0];
} PACKED;


struct multiboot_info_tag_efi_mmap
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t descriptor_size;
	multiboot_uint32_t descriptor_version;
	multiboot_uint8_t mmap[0];
} PACKED;


struct multiboot_info_tag_efi32_image_handle
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t pointer;
} PACKED;


struct multiboot_info_tag_efi64_image_handle
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint64_t pointer;
} PACKED;


struct multiboot_info_tag_load_base
{
	multiboot_uint32_t type;
	multiboot_uint32_t size;
	multiboot_uint32_t load_base_addr;
} PACKED;


struct multiboot_info
{
	multiboot_uint32_t total_size;
	multiboot_uint32_t reserved;
	struct multiboot_info_tag tags[0];
} PACKED;


#endif // KERNEL_BOOT_MULTIBOOT_HEADER

