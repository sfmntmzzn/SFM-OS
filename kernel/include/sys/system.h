
/* ----------------------------------------------------------------- *
 *
 *   system.h: basic declarations and definitions for the system.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_SYSTEM_HEADER
#define KERNEL_SYS_SYSTEM_HEADER

#include <stdint.h>


#ifndef PACKED
#define PACKED		__attribute__((packed))
#endif // PACKED


struct cpu_state
{
	uint32_t gs, fs, es, ds;
	union { uint16_t di; uint32_t edi; };
	union { uint16_t si; uint32_t esi; };
	union { uint16_t bp; uint32_t ebp; };
	union { uint16_t sp; uint32_t esp; };
	union
	{
		union { struct { uint8_t bl, bh; } PACKED; uint16_t bx; };
		uint32_t ebx;
	};
	union
	{
		union { struct { uint8_t dl, dh; } PACKED; uint16_t dx; };
		uint32_t edx;
	};
	union
	{
		union { struct { uint8_t cl, ch; } PACKED; uint16_t cx; };
		uint32_t ecx;
	};
	union
	{
		union { struct { uint8_t al, ah; } PACKED; uint16_t ax; };
		uint32_t eax;
	};
	uint32_t int_no, err_code;
	uint32_t eip, cs, eflags, useresp, ss;
} PACKED;


#endif // KERNEL_SYS_SYSTEM_HEADER
