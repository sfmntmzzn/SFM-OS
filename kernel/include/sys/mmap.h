
/* ----------------------------------------------------------------- *
 *
 *   mmap.h: declarations for the MMAP structure passed of
 *           the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_MMAP_HEADER
#define KERNEL_SYS_MMAP_HEADER

#include <stdbool.h>
#include <stdint.h>


#ifndef PACKED
#define PACKED		__attribute__((packed))
#endif // PACKED


#define MMAP_TYPE_UNDEFINED	0
#define MMAP_TYPE_FREE		1
#define MMAP_TYPE_RESERVED	2
#define MMAP_TYPE_ACPI		3
#define MMAP_TYPE_NVS		4
#define MMAP_TYPE_BADMEM	5

#define MAX_MMAP_ENTRIES	128


struct mmap_entry
{
	uint64_t base;
	uint64_t length;
	uint32_t type;
	uint32_t zero;
} PACKED;


struct mmap_info
{
	struct mmap_entry* start;
	struct mmap_entry* end;
#ifdef KERNEL_SAFETY
	bool sanitized;
#endif // KERNEL_SAFETY
};


extern struct mmap_info kernel_mmap;


void mmap_insert ( struct mmap_entry* );
void mmap_sanitize ( void );
uint32_t mmap_region_type ( uint64_t, uint64_t );
void mmap_print ( const char* );


#endif // KERNEL_SYS_MMAP_HEADER

