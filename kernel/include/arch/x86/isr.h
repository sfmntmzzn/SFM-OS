
/* ----------------------------------------------------------------- *
 *
 *   isr.h: declarations for interrupt service routines
 *          on x86 based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_ARCH_X86_ISR_HEADER
#define KERNEL_ARCH_X86_ISR_HEADER

#include <sys/system.h>

#include <stdint.h>


#define ISR_NUM_HANDLERS	32

#define ISR0	0x00
#define ISR1	0x01
#define ISR2	0x02
#define ISR3	0x03
#define ISR4	0x04
#define ISR5	0x05
#define ISR6	0x06
#define ISR7	0x07
#define ISR8	0x08
#define ISR9	0x09
#define ISR10	0x0a
#define ISR11	0x0b
#define ISR12	0x0c
#define ISR13	0x0d
#define ISR14	0x0e
#define ISR15	0x0f
#define ISR16	0x10
#define ISR17	0x11
#define ISR18	0x12
#define ISR19	0x13
#define ISR20	0x14
#define ISR21	0x15
#define ISR22	0x16
#define ISR23	0x17
#define ISR24	0x18
#define ISR25	0x19
#define ISR26	0x1a
#define ISR27	0x1b
#define ISR28	0x1c
#define ISR29	0x1d
#define ISR30	0x1e
#define ISR31	0x1f


void isr_handler ( struct cpu_state* );
void isr_set ( uint32_t, void (*)( struct cpu_state* ) );
void isr_unset ( uint32_t );
void isr_init ( void );


extern void (*kernel_isrs[ ISR_NUM_HANDLERS ])( struct cpu_state* );


#endif // KERNEL_ARCH_X86_ISR_HEADER
