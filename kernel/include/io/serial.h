
/* ----------------------------------------------------------------- *
 *
 *   serial.h: declarations and definitions regarding the
 *             serial debug output of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_IO_SERIAL_HEADER
#define KERNEL_IO_SERIAL_HEADER

#include <stdint.h>


#define SERIAL_COM1_PORT	0x3f8
#define SERIAL_COM2_PORT	0x2f8
#define SERIAL_COM3_PORT	0x3e8
#define SERIAL_COM4_PORT	0x2e8

#define SERIAL_THRE_BIT		0x20
#define SERIAL_DLAB_BIT		0x80

#define SERIAL_TXR_OFFSET	0x00
#define SERIAL_IER_OFFSET	0x01
#define SERIAL_FCR_OFFSET	0x02
#define SERIAL_LCR_OFFSET	0x03
#define SERIAL_MCR_OFFSET	0x04
#define SERIAL_LNR_OFFSET	0x05

#define SERIAL_DLL_OFFSET	0x00
#define SERIAL_DLH_OFFSET	0x01


// 300, 1200, 2400, 9600, 14400, 33600, 56000
#define DEFAULT_BAUD_RATE	9600


extern uint16_t kernel_serial_debug_port;


void serial_init ( uint16_t, int );
void serial_putchar ( char );
void serial_putstring ( const char* );


#endif // KERNEL_IO_SERIAL_HEADER

