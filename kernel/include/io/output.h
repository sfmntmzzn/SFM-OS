
/* ----------------------------------------------------------------- *
 *
 *   output.h: declarations and definitions regarding the
 *             debug output of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_IO_OUTPUT_HEADER
#define KERNEL_IO_OUTPUT_HEADER


#ifndef FORMAT
#define FORMAT(x,y)		__attribute__((format (printf,x,y)))
#endif // FORMAT


#define DEBUG_CHANNEL_SERIAL	(1<<0)
#define DEBUG_CHANNEL_TEXT	(1<<1)


extern int kernel_active_debug_mode;


void kernel_putchar ( char c );
void kernel_putstring ( const char* s );

int kernel_printf ( const char*, ... ) FORMAT(1,2);


#endif // KERNEL_IO_OUTPUT_HEADER

