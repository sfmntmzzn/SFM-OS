
/* ----------------------------------------------------------------- *
 *
 *   text.h: declarations and definitions regarding the
 *           text output of the kernel.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_IO_TEXT_HEADER
#define KERNEL_IO_TEXT_HEADER

#include <stdint.h>


void text_init ( void );
void text_putchar ( char );
void text_putstring ( const char* );


#endif // KERNEL_IO_TEXT_HEADER

